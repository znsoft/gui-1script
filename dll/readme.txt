                ********************************************
                                KiXforms 2.46

                      (c) Copyright 2005 - Freeware
                ********************************************


	Thank you for using KiXforms ! This README file contains the
	latest information about KiXforms. For further documentation 
	and usage, please refer to the online help, tutorials and sample 
	scripts located at: 

	http://www.kixforms.org

	For general discussions, questions, suggestions or to report 
	bugs, please visit the KiXforms Community Bulletin Board at:

	http://www.kixforms.org/forum


				  INSTALL
				  UPGRADE
				============

	1) Unzip the contents of this archive into a seperate directory

	2) Start a DOS prompt, go to that directory and enter the
	   following command:

	   c:\> regsvr32 kixforms.dll
	
	3) To un-install Kixforms, enter the following command:

	   c:\> regsvr32 -u kixforms.dll

	4) To upgrade to a newer version, simple replace the newer DLL
	   over the older DLL and re-register with regsvr32.







