﻿//***********************************************************************
//===СЦЕНАРИЙ ОБЕСПЕЧЕНИЯ РАБОТЫ KIXFORMS===201601151504==============
Перем Shell Экспорт, ТЗЦвета Экспорт, Кикстарт Экспорт;

Функция ИзHexВ_RGB(Знач ЗнHex)
	Ж = Новый Массив;
	Для А=-6 По -1 Цикл
		Ж.Добавить(СтрНайти("0123456789ABCDEF",Сред(ЗнHex,-А,1))-1);
	КонецЦикла;
	М = Новый Массив;
	М.Добавить((Ж[1]*16)+Ж[0]);
	М.Добавить((Ж[3]*16)+Ж[2]);
	М.Добавить((Ж[5]*16)+Ж[4]);
	Возврат М;
КонецФункции

Функция ИзDecВ_Hex(Знач ЗнDec)
	Рез = ""; 
	Для А = -6 По -1 Цикл
		Рез = Сред("0123456789ABCDEF", ЗнDec%16 + 1, 1) + Рез; 
		ЗнDec = Цел(ЗнDec/16); 
	КонецЦикла;
	Возврат Рез;
КонецФункции

Функция ПодборПохожегоЦвета(ВыбранныйЦвет) Экспорт
	М = ИзHexВ_RGB(ИзDecВ_Hex(ВыбранныйЦвет));
	Для А = 0 По ТЗЦвета.Количество() - 1 Цикл
		СтрТЗ = ТЗЦвета.Получить(А);
		СтрТЗ.ДэльтаRGB = Макс(-(М[0] - СтрТЗ.ЦветR), (М[0] - СтрТЗ.ЦветR)) + Макс(-(М[1] - СтрТЗ.ЦветG), (М[1] - СтрТЗ.ЦветG)) + Макс(-(М[2] - СтрТЗ.ЦветB), (М[2] - СтрТЗ.ЦветB));
	КонецЦикла;
	ТЗЦвета.Сортировать("ДэльтаRGB, ЦветR, ЦветG, ЦветB");
	Для А = 0 По ТЗЦвета.Количество() - 1 Цикл
		СтрТЗ = ТЗЦвета.Получить(А);
		Если НЕ (СтрТЗ.ЦветR = 0 И СтрТЗ.ЦветG = 0 И СтрТЗ.ЦветB = 0) Тогда
			Возврат ТЗЦвета.Получить(А).ЦветИмя;
		КонецЕсли;
	КонецЦикла;
КонецФункции

Скрипт = Новый COMОбъект("MSScriptControl.ScriptControl");
Скрипт.Language = "vbscript";
Стр = "
|Function GetKix()
| Set Kix = CreateObject(""Kixtart.System"")
| Set GetKix =  Kix
|End Function"; 
Скрипт.AddCode(Стр);
Кикстарт = Скрипт.Run("GetKix");

Shell = Новый COMОбъект("Wscript.Shell");

// обеспечим поддержку цвета
ТЗЦвета = Новый ТаблицаЗначений;
Стр = "ЦветИмя,ЦветHex,ЦветR,ЦветG,ЦветB,ЦветDec,ДэльтаRGB";
Стр = СтрЗаменить(Стр,",",Символы.ПС);
Для А = 1 По СтрЧислоСтрок(Стр) Цикл
	ТЗЦвета.Колонки.Добавить(СтрПолучитьСтроку(Стр,А));
КонецЦикла;
Стр = "AliceBlue,AntiqueWhite,Aqua,Aquamarine,Azure,Beige,Bisque,Black,BlanchedAlmond,Blue,BlueViolet,Brown,BurlyWood,CadetBlue,Chartreuse,Chocolate,Coral,CornflowerBlue,Cornsilk,Crimson,Cyan,DarkBlue,DarkCyan,DarkGoldenrod,DarkGray,DarkGreen,DarkKhaki,DarkMagenta,DarkOliveGreen,DarkOrange,DarkOrchid,DarkRed,DarkSalmon,DarkSeaGreen,DarkSlateBlue,DarkSlateGray,DarkTurquoise,DarkViolet,DeepPink,DeepSkyBlue,DimGray,DodgerBlue,Firebrick,FloralWhite,ForestGreen,Fuchsia,Gainsboro,GhostWhite,Gold,Goldenrod,Gray,Green,GreenYellow,Honeydew,HotPink,IndianRed,Indigo,Ivory,Khaki,Lavender,LavenderBlush,LawnGreen,LemonChiffon,LightBlue,LightCoral,LightCyan,LightGoldenrodYellow,LightGray,LightGreen,LightPink,LightSalmon,LightSeaGreen,LightSkyBlue,LightSlateGray,LightSteelBlue,LightYellow,Lime,LimeGreen,Linen,Magenta,Maroon,MediumAquamarine,MediumBlue,MediumOrchid,MediumPurple,MediumSeaGreen,MediumSlateBlue,MediumSpringGreen,MediumTurquoise,MediumVioletRed,MidnightBlue,MintCream,MistyRose,Moccasin,NavajoWhite,Navy,OldLace,Olive,OliveDrab,Orange,OrangeRed,Orchid,PaleGoldenrod,PaleGreen,PaleTurquoise,PaleVioletRed,PapayaWhip,PeachPuff,Peru,Pink,Plum,PowderBlue,Purple,Red,RosyBrown,RoyalBlue,SaddleBrown,Salmon,SandyBrown,SeaGreen,SeaShell,Sienna,Silver,SkyBlue,SlateBlue,SlateGray,Snow,SpringGreen,SteelBlue,Tan,Teal,Thistle,Tomato,Turquoise,Violet,Wheat,White,WhiteSmoke,Yellow,YellowGreen,ActiveBorder,ActiveCaption,ActiveCaptionText,AppWorkspace,Control,ControlDark,ControlDarkDark,ControlLight,ControlLightLight,ControlText,Desktop,GrayText,Highlight,HighlightText,HotTrack,InactiveBorder,InactiveCaption,InactiveCaptionText,Info,InfoText,Menu,MenuText,ScrollBar,Window,WindowFrame,WindowText,Transparent";
Стр = СтрЗаменить(Стр,",",Символы.ПС);
Для А = 1 По СтрЧислоСтрок(Стр) Цикл
	ТЗЦвета.Добавить().ЦветИмя = СтрПолучитьСтроку(Стр,А);
КонецЦикла;

Форма1 = Кикстарт.Form();
Для А = 0 По ТЗЦвета.Количество() - 1 Цикл
	СтрТЗ = ТЗЦвета.Получить(А);
	Форма1.BackColor = СтрТЗ.ЦветИмя;
	ЦветDec = Форма1.BackColor;
	СтрТЗ.ЦветDec = ЦветDec;
	ЦветHex = ИзDecВ_Hex(ЦветDec);
	СтрТЗ.ЦветHex = ЦветHex;
	М = ИзHexВ_RGB(ЦветHex);
	СтрТЗ.ЦветR = М[0];
	СтрТЗ.ЦветG = М[1];
	СтрТЗ.ЦветB = М[2];
КонецЦикла;
Сообщить("Сценарий поддержки 201601151504 запущен");
//***********************************************************************
