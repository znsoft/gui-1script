﻿//***********************************************************************
Перем ПоддержкаKiXforms; // переменная, необходимая всегда для поддержки KiXforms
Перем Кикс, Форма1, Надпись1, Надпись2, ПолеКартинки1, КоллекцияИзображений1, НайденныеФайлы, ТекущийИндекс, Кнопка1, Кнопка2, Кнопка3, Стрелки; // переменные для этого сценария

Процедура СоздатьФорму()
	Форма1 = Кикс.Form();
	Форма1.Text = "Пример: PictureBox";
	Форма1.Size = Кикс.Size(400, 460);
	Форма1.Resizable = Ложь;
	
	Надпись1 = Форма1.Label();
	Надпись1.TextAlign = "MiddleCenter";
	Надпись1.Width = 120;
	Надпись1.Height = 25;
	Надпись1.Center();
	Надпись1.Top = 5;
	Надпись1.Text = "0/0";
	
	ПолеКартинки1 = Форма1.Controls.Add("PictureBox");
	ПолеКартинки1.Size = Кикс.Size(300, 300);
	ПолеКартинки1.Center();
	ПолеКартинки1.Top = Надпись1.Bottom + 10;
	ПолеКартинки1.SizeMode = 1;
	
	Кнопка1 = Форма1.Button();
	Кнопка1.Text = "Каталог картинок (*.jpg)";
	Кнопка1.Width = 170;
	Кнопка1.Height = 25;
	Кнопка1.Center();
	Кнопка1.Top = ПолеКартинки1.Bottom + 10;
	Кнопка1.OnClick = "Каталог()";
	
	Надпись2 = Форма1.Label();
	Надпись2.Left = 10;
	Надпись2.Top = Кнопка1.Bottom + 5;
	Надпись2.Width = 380;
	Надпись2.Height = 80;
	
	// берём маленькие значки из формы Форма1.SmallImageList()
	Стрелки = Форма1.ImageList(); // создаем список больших значков из двух элементов на основе списка маленьких
	Стрелки.ImageSize = Кикс.Size(32,32);
	Стрелки.Images.Add(Форма1.SmallImageList.Images(18)); // это стрелка налево
	Стрелки.Images.Add(Форма1.SmallImageList.Images(19)); // это стрелка направо
	
	Кнопка2 = Форма1.Button();
	Кнопка2.Icon = Стрелки.Images(0);
	
	Кнопка2.Left = 9;
	Кнопка2.Top = (ПолеКартинки1.Height / 2) + 10;
	Кнопка2.Width = 32;
	Кнопка2.Height = 32;
	Кнопка2.OnClick = "Влево()";
	
	Кнопка3 = Форма1.Button();
	Кнопка3.Icon = Стрелки.Images(1);
	
	Кнопка3.Left = ПолеКартинки1.Right + 9;
	Кнопка3.Top = (ПолеКартинки1.Height / 2) + 10;
	Кнопка3.Width = 32;
	Кнопка3.Height = 32;
	Кнопка3.OnClick = "Вправо()";
	
	ПолеКартинки1.Image = Кикс.Bitmap(ПолеКартинки1.Size);
КонецПроцедуры

Процедура ВыборФункции(Действие)
	ПоддержкаKiXforms.Shell.SendKeys(""); // без этой строки, в некоторых случаях, форма будет реагировать на события с задержкой!
	Если Действие = "Каталог()" Тогда
		Каталог();
	ИначеЕсли Действие = "Влево()" Тогда
		Влево(); 
	ИначеЕсли Действие = "Вправо()" Тогда
		Вправо(); 
	КонецЕсли; 
КонецПроцедуры

Функция Каталог()
	ДиалогВыбораКаталога1 = Кикс.FolderBrowserDialog();
	Надпись1.Text = "0/0";
	Надпись2.Text = "";
	ТекущийИндекс = Неопределено;
	ПолеКартинки1.Image = Кикс.Bitmap(ПолеКартинки1.Size);
	Каталог = "";
	Если ДиалогВыбораКаталога1.ShowDialog() = 1 Тогда
		Каталог = ДиалогВыбораКаталога1.SelectedPath;
		КоллекцияИзображений1 = Кикс.ImageList();
		НайденныеФайлы = НайтиФайлы(Каталог, "*.jpg", Ложь);
		Если НайденныеФайлы.Количество() > 0 Тогда
			ПолеКартинки1.Image = НайденныеФайлы.Получить(0).ПолноеИмя;
			Надпись2.Text = НайденныеФайлы.Получить(0).ПолноеИмя;
			ТекущийИндекс = 0;
			Надпись1.Text = "" + (ТекущийИндекс + 1) + "/" + НайденныеФайлы.Количество();
			// важно обновить надписи и иконки на кнопках, иначе они могут потеряться, перестать прорисовываться вместе с самой кнопкой
			Кнопка1.Text = "Каталог картинок (*.jpg)";
			Кнопка2.Icon = Стрелки.Images(0);
			Кнопка3.Icon = Стрелки.Images(1);
		КонецЕсли;
	КонецЕсли;
	ПолеКартинки1.Refresh();
КонецФункции

Функция Влево()
	Попытка
		ИндексЛевее = ТекущийИндекс - 1;
		Если ИндексЛевее >= 0 Тогда
			ПолеКартинки1.Image = НайденныеФайлы.Получить(ИндексЛевее).ПолноеИмя;
			Надпись2.Text = НайденныеФайлы.Получить(ИндексЛевее).ПолноеИмя;
			ТекущийИндекс = ИндексЛевее;
			Надпись1.Text = "" + (ТекущийИндекс + 1) + "/" + НайденныеФайлы.Количество();
			ПолеКартинки1.Refresh();
			// важно обновить надписи и иконки на кнопках, иначе они могут потеряться, перестать прорисовываться вместе с самой кнопкой
			Кнопка1.Text = "Каталог картинок (*.jpg)";
			Кнопка2.Icon = Стрелки.Images(0);
			Кнопка3.Icon = Стрелки.Images(1);
		КонецЕсли;
	Исключение
	КонецПопытки;
КонецФункции

Функция Вправо()
	Попытка
		ИндексПравее = ТекущийИндекс + 1;
		Если ИндексПравее <= НайденныеФайлы.ВГраница() Тогда
			ПолеКартинки1.Image = НайденныеФайлы.Получить(ИндексПравее).ПолноеИмя;
			Надпись2.Text = НайденныеФайлы.Получить(ИндексПравее).ПолноеИмя;
			ТекущийИндекс = ИндексПравее;
			Надпись1.Text = "" + (ТекущийИндекс + 1) + "/" + НайденныеФайлы.Количество();
			ПолеКартинки1.Refresh();
			// важно обновить надписи и иконки на кнопках, иначе они могут потеряться, перестать прорисовываться вместе с самой кнопкой
			Кнопка1.Text = "Каталог картинок (*.jpg)";
			Кнопка2.Icon = Стрелки.Images(0);
			Кнопка3.Icon = Стрелки.Images(1);
		КонецЕсли;
	Исключение
	КонецПопытки;
КонецФункции

Процедура ПриОткрытии()
	Стр201601151504 = "UEsDBBQAAgAIAHmtL0glqYJS2gcAAJMVAAASAAAAU3VwcG9ydEtpWGZvcm1zLm9zzVhbbxvHFX62AP2HBYECZDKiRMdWUwh6EKlIFixWgihUCYogGHFH4obLGXq5K5lFHhQLddAmRVQjTtxATaKgSB6KorJrx7JlyX9h+EvyF/LNhXuhRCZt8xA+zJ4zc843Z85lLvz+9OXk5Cs/z298bHJydnZWHslv5H35d3kgv5IP5N8c+YX8KzqO5Jdov9VDD+S/HYweYOAL+bX8p3Nz6c2FlbVqDfpXp0rTU6XS9dL1qWuzmd/4GCCe9PbkE/nCqTWY7zvyX/J57335Up739np3iAOwzzD9I4jdkccXhz+XJ6pHDaqeAYGZ8THM8Y/evjxD91150vvYga1Pb7Db8t47a4vlPNDPoPqBowh0F8bHrshPnVkHizqXj3ofymeOPITE+0A9kY9m1Pgn8lQBHcxOTDtYwbkzUXJgJCyRpxgHQBEy5/KhPIblJ707vY/y8gifPaAey2ew9iSfmypdfe3a9elfv/6buXJl/o2FHIGM8sXjvDWGTMgDUioUJkoFPe/nwDyDJ+72J5PHuv/wR+w9vGhOXn76+9Lbr5SmC6+Cmnq7MErwtVjw6mjB67HgNSt4D1JPYdceTEJ0DtGZWkYqMvJkSKzmWR2xgjeysUK3jtVXcNhTrD+Xm3HSsUHXsOj0VWJ3XxKK/hy/Kk07rzol4pQK+BpVPdEVK6BwvkHvad52TMIDxpRh4RrwicH87/yCZcnHcD+yXNG9P4L/Djj/wTeulry8h3x4qOaB7pnKjf5gYaBQ4izKFkfW/cPRCjMDrp8yns9Wb1Gv8BQr+AAdqmYfyfN8wZlwBgJkSgW6yqIBCLXy094+IGxdHejJU0pIzd5fIPORUsAyFAgqQi03P5GXh8h2TJmWt/BrBeKMHtc5kMEqDZFdtFjDxy9iXR0iW7ZYw8dHbw8DHjwyIYf/9tSWoZIQfswNeI" +
	"048bITcjEhy7lfQtDvI4WBrXa/+07+spgZux5cGoMRYyprpgrKkHMU1WN5rKa7ULo/bmYMiFp60fvYWJ0Eyto/Inwjd4UjcHvgXsKYzBFQWaniIH7Y+zP2hOe9O/lctVarB147rAgeBsIvZjgdyjRYcZny7YhuM7Wx7mx2tHBOCylPqd7xsfcWIl4PPcGdRRbe9G7nsR+/d6XGQgcMRCoBoyFb2XyX1cN8LofOkAZhsdbthKyVy8XSRhsKSg+db3DX6UPnZq4MWDbnuhXhMhvrgvFQ9g6gdvaUxlrE8zkziV7o+Ji5aox22IZZdFHLmlxX9yFHn3pP9M75RAUaNxe1iSJFHuurzHdQ33cQI5sVsC97hcke1V9jA3ioy+MuKHvAwYYzID/LODybSKTPqmtCUqlJocaJHFPYyclAkWdCaqvgM9jxwliQuriQHFH3E7VeVcDA+FDl+5EOQGoPKNk9wGB9CwyV4OeWP5fP+4FLF//lO4ZK++fyZOhVaqDakil6+32jUYCFbA1lSiv27Zzv1VnZjxiZ46F3K2IbDS8EcyuiumnRwOPg/xAFjJSZt43W60COlH1ab6qW1xvMnfNbgrtEI6nmd57wWUjKgdjlpBwFfndDCJdUqMtCLVRpoCQCFnUUKerCR8WQigior1q+5YtdFhhJsB3Pb5JK4LU6gpNKl3IyT4OmHlZE3LMofJfxADNpLqBdSzBmBG42aNPTVBVFzkOq6RXf22GJ0EqATYBZst7wDNoaM98aVWs1JKOJVk0tITZJc7EB61FwKxJex4xZ58wz1l71eFMTtWbX6HotoyXcbeuABS9gm4EHby/4ykEmRAsiYJ3QTL8Q1Rsdj5JF6vHOpggEWWyITmgElU9I4hiNbtR0+xaqXOySG4KzrssUEWqjlrjrUa4WrahtQZZ2RNAlxoHLdIdxlwUxAUM7DXC73EAvM/io0vC2tuCqZW+7YaKuKRNlQ6rAaSo20NpjOpWtltKoitTW" +
	"acpGwtD9UBjOetMwcSgMGzLmJ6PxfC2mmz4KR9vPkSoNBCaqMteLWqmyMB0ay5A2XQyzGgVtvz8S22fZOFks3wbgdloiSRnDm6RR8ah6Lo89WvV4qM6bFqhO2F0TSkHU67TjcfJbukPfFSYNQHfJiu8u0zojOuFNOx/QTWIz3nzUHHYhq9RnSepoTpuoqMRAxSXmrdI27VJM2iarjNYbq9HWFqggIjpyq37UIqti17XZbb2kNGF81+wYa6JLTYxq1HV9ZnptvGuUu1Yu9ioIfV6Rmsc4p/j4O0jPfh4k7k6SocYR9rTfk8RYR1auMyTpegNOhXXrokVDQZIl2xLeaOCkJ8bBuq21RJMRm1PmY8DncKrvsLIIVNUYpkLb6qTPcuvsdkjm2u0NETQ7bRUre1Xpf9UGkqbTvE7oDJPp0djzrNMMRVvvA7rjBkR8LRZTpl+E64Ha45c4TRvfZ/vmD/Bad4lvCd1orsp4pBvN4QImfL9MA7LhcRduMp+FgKICDa3lMDmHBwJU4C/0tB48ngfvvImxP+3EHn5gq3ewfvK+kMclBTtw8yviNGjlBxf5/zxL/pdXScrGYhmZUxE4rxInpF8ZyaMgdUlTopdBzGTsKQ5oJFwaDrdD+6pPveMT0cIQTKuWcDND/iBIJIZBrennN97UQ8YXzXhp2Lh5vuPtO+oeh27kYO9PNhA54NzVRXGsXwHPLlzR8WRM/y3p4FF3jMfCPiCgpq/7P9vfqD8AUEsBAhQAFAACAAgAea0vSCWpglLaBwAAkxUAABIAAAAAAAAAAQAgAAAAAAAAAFN1cHBvcnRLaVhmb3Jtcy5vc1BLBQYAAAAAAQABAEAAAAAKCAAAAAA=";
	ДД = Base64Значение(Стр201601151504);
	ДД.Записать(КаталогВременныхФайлов() + "SupportKiXforms.zip");
	ЧтениеZip = Новый ЧтениеZipФайла(КаталогВременныхФайлов() + "SupportKiXforms.zip");
	ЧтениеZip.ИзвлечьВсе(КаталогВременныхФайлов(), РежимВосстановленияПутейФайловZIP.НеВосстанавливать);
	ЧтениеZip.Закрыть(); 
	ПодключитьСценарий(КаталогВременныхФайлов() + "SupportKiXforms.os", "СценарийKiXforms");
	ПоддержкаKiXforms = Новый СценарийKiXforms();
	Кикс = ПоддержкаKiXforms.Кикстарт;
	Если НЕ (Кикс.Version = "2.47.4.0") Тогда
		ЗавершитьРаботу(1);
	КонецЕсли;
	
	СоздатьФорму();
	
	Форма1.Center();
	Форма1.Visible = Истина;
	Форма1.Show();
	
	Пока Форма1.Visible Цикл
		Кикс = ПоддержкаKiXforms.Кикстарт; // очень важная строка, без неё GUI будет вылетать
		Действие = Кикс.DoEvents();
		Форма1.Enabled = Ложь;
		Кикс.Sender.Enabled = Ложь;
		ВыборФункции(Действие);
		Форма1.Enabled = Истина;
		Кикс.Sender.Enabled = Истина;
		Кикс.Sender.SetFocus();
		ПоддержкаKiXforms.Shell.SendKeys(""); // без этой строки, в некоторых случаях, форма будет реагировать на события с задержкой!
		Действие = "";
	КонецЦикла;
КонецПроцедуры

ПриОткрытии();
//***********************************************************************
