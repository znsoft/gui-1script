﻿//***********************************************************************
Перем ПоддержкаKiXforms; // переменная, необходимая всегда для поддержки KiXforms
Перем Кикс, Форма1, ПанельСвойств, Панель2, ПанельКомпонентов, ПанельФорм, Разделитель1, Разделитель2, Кнопка10, Кнопка11, Кнопка12, КоллекцияИконок;

Процедура СоздатьФорму()
	Форма1 = Кикс.Form();
	Форма1.Size = Кикс.Size(1500,800); //(Ширина,Высота);
	Форма1.Text = "OneScript SplitContainer";
	Форма1.MaximizeBox = Ложь;
	
	КоллекцияИконок= Кикс.ImageList(); // создаем коллекцию больших значков на основе коллекции маленьких
	КоллекцияИконок.ImageSize = Кикс.Size(32, 32);
	Для А = 0 По 109 Цикл
		КоллекцияИконок.Images.Add(Кикс.BuiltinIcons(А));
	КонецЦикла;
	
	СоздатьМеню();
	
	ПанельСвойств = Форма1.Controls.Add("Panel");
	ПанельСвойств.Width = 300;// ширину нужно задавать до присоединения панели на форме, то есть, до определения свойства Dock
	ПанельСвойств.Dock = "Right";
	ПанельСвойств.BackColor = "Aqua";
	
	Разделитель1 = Форма1.Controls.Add("Splitter");
	Разделитель1.Dock = "Right";
	Разделитель1.BackColor = "Blue";
	
	Панель2 = Форма1.Controls.Add("Panel");
	Панель2.Dock = "Fill";
	Панель2.BackColor = "DarkKhaki";
	
	ПанельКомпонентов = Панель2.Controls.Add("Panel");
	ПанельКомпонентов.Dock = "Left";
	ПанельКомпонентов.BackColor = "DarkViolet";
	ПанельКомпонентов.Width = 150;
	ПанельКомпонентов.OnResize = "ПанельКомпонентов_OnResize()";
	
	Разделитель2 = Панель2.Controls.Add("Splitter");
	Разделитель2.Dock = "Left";
	Разделитель2.BackColor = "Blue";
	
	ПанельФорм = Панель2.Controls.Add("Panel");
	ПанельФорм.Dock = "Fill";
	ПанельФорм.BackColor = "Red";
	ПанельФорм.OnResize = "ПанельФорм_OnResize()";
	
	Форма1.MinimumSize = Кикс.Size(Число(ПанельСвойств.Size.Width) + Число(ПанельКомпонентов.Size.Width) + 40, 200);  
КонецПроцедуры

Процедура СоздатьМеню()
	ГлавноеМеню = Кикс.MainMenu();
	Подменю1  = ГлавноеМеню.MenuItems.Add("Файл");         
	Подменю11 = Подменю1.MenuItems.Add("Новый"           , "Подменю11_OnClick()",,"" );
	Подменю12 = Подменю1.MenuItems.Add("Открыть"         , "Подменю12_OnClick()",,"" );
	Подменю13 = Подменю1.MenuItems.Add("Сохранить"       , "Подменю13_OnClick()",,"-");
	Подменю14 = Подменю1.MenuItems.Add("Сохранить как...", "Подменю14_OnClick()",,"" );
	Подменю15 = Подменю1.MenuItems.Add("Генерировать код", "Подменю15_OnClick()",,"-");
	Подменю16 = Подменю1.MenuItems.Add("Выход"           , "Подменю16_OnClick()",,"-");
	
	Подменю2  = ГлавноеМеню.MenuItems.Add("Вид");
	Подменю21 = Подменю2.MenuItems.Add("Форма",,,"");
	Подменю22 = Подменю2.MenuItems.Add("Код",,,"");
	
	Подменю3  = ГлавноеМеню.MenuItems.Add("Инструменты");
	Подменю31 = Подменю3.MenuItems.Add("Конструктор формы",,,"");
	Подменю32 = Подменю3.MenuItems.Add("Генерировать код",,,"-");
	Подменю33 = Подменю3.MenuItems.Add("Генерировать/запустить код",,,"");
	Подменю34 = Подменю3.MenuItems.Add("Настройки",,,"-");
	
	Подменю4  = ГлавноеМеню.MenuItems.Add("Справка");
	Подменю41 = Подменю4.MenuItems.Add("Справка"       ,,,"");
	Подменю41 = Подменю4.MenuItems.Add("О программе...",,,"");
	
	Форма1.Menu = ГлавноеМеню;
	
	ПанельИнструментов1 = Форма1.Controls.Add("Panel");
	ПанельИнструментов1.BorderStyle = 1;
	ПанельИнструментов1.Dock = 1;
	ПанельИнструментов1.Height = 38;
	
	Кнопка1 = ПанельИнструментов1.Controls.Add("ToolButton");
	Кнопка1.Dock = "Left";
	Кнопка1.Width = 40;
	Кнопка1.BackColor = "Aqua";
	Кнопка1.Style = 0;
	Кнопка1.Pushed = Ложь;
	Кнопка1.Text = "";
	Кнопка1.Icon = КоллекцияИконок.Images(0);
	Кнопка1.ToolTipText = "Новая форма";
	Кнопка1.OnClick = "Кнопка1_Click()";
	
	Кнопка2 = ПанельИнструментов1.Controls.Add("ToolButton");
	Кнопка2.Dock = "Left";
	Кнопка2.Width = 40;
	Кнопка2.BackColor = "Aqua";
	Кнопка2.Style = 0;
	Кнопка2.Pushed = Ложь;
	Кнопка2.Text = "";
	Кнопка2.Icon = КоллекцияИконок.Images(1);
	Кнопка2.ToolTipText = "Открыть проект";
	Кнопка2.OnClick = "Кнопка2_Click()";
	
	Кнопка3 = ПанельИнструментов1.Controls.Add("ToolButton");
	Кнопка3.Dock = "Left";
	Кнопка3.Width = 40;
	Кнопка3.BackColor = "Aqua";
	Кнопка3.Style = 0;
	Кнопка3.Pushed = Ложь;
	Кнопка3.Text = "";
	Кнопка3.Icon = КоллекцияИконок.Images(3);
	Кнопка3.ToolTipText = "Сохранить проект";
	Кнопка3.OnClick = "Кнопка3_Click()";
	
	Кнопка4 = ПанельИнструментов1.Controls.Add("ToolButton");
	Кнопка4.Dock = "Left";
	Кнопка4.Width = 40;
	Кнопка4.BackColor = "Aqua";
	Кнопка4.Style = 0;
	Кнопка4.Pushed = Ложь;
	Кнопка4.Text = "Кн4";
	Кнопка4.ToolTipText = "Кн4";
	Кнопка4.OnClick = "Кнопка4_Click()";
	
	Кнопка5 = ПанельИнструментов1.Controls.Add("ToolButton");
	Кнопка5.Dock = "Left";
	Кнопка5.Width = 40;
	Кнопка5.BackColor = "Aqua";
	Кнопка5.Style = 0;
	Кнопка5.Pushed = Ложь;
	Кнопка5.Text = "Кн5";
	Кнопка5.ToolTipText = "Кн5";
	Кнопка5.OnClick = "Кнопка5_Click()";
	
	Кнопка6 = ПанельИнструментов1.Controls.Add("ToolButton");
	Кнопка6.Dock = "Left";
	Кнопка6.Width = 40;
	Кнопка6.BackColor = "Aqua";
	Кнопка6.Style = 0;
	Кнопка6.Pushed = Ложь;
	Кнопка6.Text = "Кн6";
	Кнопка6.ToolTipText = "Кн6";
	Кнопка6.OnClick = "Кнопка6_Click()";
	
	Кнопка7 = ПанельИнструментов1.Controls.Add("ToolButton");
	Кнопка7.Dock = "Left";
	Кнопка7.Width = 40;
	Кнопка7.BackColor = "Aqua";
	Кнопка7.Style = 0;
	Кнопка7.Pushed = Ложь;
	Кнопка7.Text = "Кн7";
	Кнопка7.ToolTipText = "Кн7";
	Кнопка7.OnClick = "Кнопка7_Click()";
	
	Кнопка8 = ПанельИнструментов1.Controls.Add("ToolButton");
	Кнопка8.Dock = "Left";
	Кнопка8.Width = 40;
	Кнопка8.BackColor = "Aqua";
	Кнопка8.Style = 0;
	Кнопка8.Pushed = Ложь;
	Кнопка8.Text = "Кн8";
	Кнопка8.ToolTipText = "Кн8";
	Кнопка8.OnClick = "Кнопка8_Click()";
	
	Кнопка9 = ПанельИнструментов1.Controls.Add("ToolButton");
	Кнопка9.Dock = "Left";
	Кнопка9.Width = 40;
	Кнопка9.BackColor = "Aqua";
	Кнопка9.Style = 0;
	Кнопка9.Pushed = Ложь;
	Кнопка9.Text = "Кн9";
	Кнопка9.ToolTipText = "Кн9";
	Кнопка9.OnClick = "Кнопка9_Click()";
	
	Кнопка10 = ПанельИнструментов1.Controls.Add("ToolButton");
	Кнопка10.Dock = "Left";
	Кнопка10.Width = 40;
	Кнопка10.BackColor = "Aqua";
	Кнопка10.Style = 0;
	Кнопка10.Pushed = Ложь;
	Кнопка10.Text = "Кн10";
	Кнопка10.ToolTipText = "Кн10";
	Кнопка10.OnClick = "Кнопка10_Click()";
	
	Кнопка11 = ПанельИнструментов1.Controls.Add("ToolButton");
	Кнопка11.Dock = "Left";
	Кнопка11.Width = 40;
	Кнопка11.BackColor = "White";
	Кнопка11.Style = 1;
	Кнопка11.BorderStyle = 2;
	Кнопка11.Pushed = Истина;
	Кнопка11.Text = "";
	Кнопка11.Icon = КоллекцияИконок.Images(23);
	Кнопка11.ToolTipText = "Элементы";
	Кнопка11.OnClick = "Кнопка11_Click()";
	
	Кнопка12 = ПанельИнструментов1.Controls.Add("ToolButton");
	Кнопка12.Dock = "Left";
	Кнопка12.Width = 40;
	Кнопка12.BackColor = "White";
	Кнопка12.Style = 0;
	Кнопка12.Pushed = Истина;
	Кнопка12.Text = "";
	Кнопка12.Icon = КоллекцияИконок.Images(41);
	Кнопка12.ToolTipText = "Панель свойств";
	Кнопка12.OnClick = "Кнопка12_Click()";
	
	Кнопка13 = ПанельИнструментов1.Controls.Add("ToolButton");
	Кнопка13.Dock = "Left";
	Кнопка13.Width = 40;
	Кнопка13.BackColor = "Aqua";
	Кнопка13.Style = 0;
	Кнопка13.Pushed = Ложь;
	Кнопка13.Text = "";
	Кнопка13.Icon = КоллекцияИконок.Images(92);
	Кнопка13.ToolTipText = "Настройки";
	Кнопка13.OnClick = "Кнопка13_Click()";
	
	ПанельСтатуса1 = Форма1.StatusBar();
	ПанельСтатуса1.Dock = 2;
КонецПроцедуры

Процедура ВыборФункции(Действие)
	ПоддержкаKiXforms.Shell.SendKeys(""); // это нужно для устранения задержки реакции на события, точнее задержки отрисовки формы
	Если Действие = "Подменю11_OnClick()" Тогда
		Подменю11_OnClick();
	ИначеЕсли Действие = "Подменю12_OnClick()" Тогда
		Подменю12_OnClick();
	ИначеЕсли Действие = "Подменю13_OnClick()" Тогда
		Подменю13_OnClick();
	ИначеЕсли Действие = "Подменю14_OnClick()" Тогда
		Подменю14_OnClick();
	ИначеЕсли Действие = "Подменю15_OnClick()" Тогда
		Подменю15_OnClick();
	ИначеЕсли Действие = "Подменю16_OnClick()" Тогда
		Подменю16_OnClick();
	ИначеЕсли Действие = "Кнопка1_Click()" Тогда
		Кнопка1_Click();
	ИначеЕсли Действие = "Кнопка2_Click()" Тогда
		Кнопка2_Click();
	ИначеЕсли Действие = "Кнопка3_Click()" Тогда
		Кнопка3_Click();
	ИначеЕсли Действие = "Кнопка4_Click()" Тогда
		Кнопка4_Click();
	ИначеЕсли Действие = "Кнопка5_Click()" Тогда
		Кнопка5_Click();
	ИначеЕсли Действие = "Кнопка6_Click()" Тогда
		Кнопка6_Click();
	ИначеЕсли Действие = "Кнопка7_Click()" Тогда
		Кнопка7_Click();
	ИначеЕсли Действие = "Кнопка8_Click()" Тогда
		Кнопка8_Click();
	ИначеЕсли Действие = "Кнопка9_Click()" Тогда
		Кнопка9_Click();
	ИначеЕсли Действие = "Кнопка10_Click()" Тогда
		Кнопка10_Click();
	ИначеЕсли Действие = "Кнопка11_Click()" Тогда
		Кнопка11_Click();
	ИначеЕсли Действие = "Кнопка12_Click()" Тогда
		Кнопка12_Click();
	ИначеЕсли Действие = "Кнопка13_Click()" Тогда
		Кнопка13_Click();
	ИначеЕсли Действие = "ПанельФорм_OnResize()" Тогда
		ПанельФорм_OnResize();
	ИначеЕсли Действие = "ПанельКомпонентов_OnResize()" Тогда
		ПанельКомпонентов_OnResize();
	КонецЕсли;
КонецПроцедуры

Функция Подменю11_OnClick()
	Кикс.MessageBox.Show("Нажата: " + Кикс.Sender.Text);
КонецФункции

Функция Подменю12_OnClick()
	Кикс.MessageBox.Show("Нажата: " + Кикс.Sender.Text);
КонецФункции

Функция Подменю13_OnClick()
	Кикс.MessageBox.Show("Нажата: " + Кикс.Sender.Text);
КонецФункции

Функция Подменю14_OnClick()
	Кикс.MessageBox.Show("Нажата: " + Кикс.Sender.Text);
КонецФункции

Функция Подменю15_OnClick()
	Кикс.MessageBox.Show("Нажата: " + Кикс.Sender.Text);
КонецФункции

Функция Подменю16_OnClick()
	Кикс.MessageBox.Show("Нажата: " + Кикс.Sender.Text);
	Форма1.Visible = Ложь;
КонецФункции

Функция Кнопка1_Click()
	Кикс.MessageBox.Show("Нажата: " + Кикс.Sender.Text);
КонецФункции

Функция Кнопка2_Click()
	Кикс.MessageBox.Show("Нажата: " + Кикс.Sender.Text);
КонецФункции

Функция Кнопка3_Click()
	Кикс.MessageBox.Show("Нажата: " + Кикс.Sender.Text);
КонецФункции

Функция Кнопка4_Click()
	Кикс.MessageBox.Show("Нажата: " + Кикс.Sender.Text);
КонецФункции

Функция Кнопка5_Click()
	Кикс.MessageBox.Show("Нажата: " + Кикс.Sender.Text);
КонецФункции

Функция Кнопка6_Click()
	Кикс.MessageBox.Show("Нажата: " + Кикс.Sender.Text);
КонецФункции

Функция Кнопка7_Click()
	Кикс.MessageBox.Show("Нажата: " + Кикс.Sender.Text);
КонецФункции

Функция Кнопка8_Click()
	Кикс.MessageBox.Show("Нажата: " + Кикс.Sender.Text);
КонецФункции

Функция Кнопка9_Click()
	Кикс.MessageBox.Show("Нажата: " + Кикс.Sender.Text);
КонецФункции

Функция Кнопка10_Click()
	Кикс.MessageBox.Show("Нажата: " + Кикс.Sender.Text);
КонецФункции

Функция Кнопка11_Click()
	Видимость = ПанельКомпонентов.Visible;
	ПанельКомпонентов.Visible = НЕ Видимость;
	Если Видимость Тогда
		ПанельФорм.Size = Кикс.Size((Число(ПанельКомпонентов.Size.Width) + Число(ПанельФорм.Size.Width)), ПанельФорм.Size.Height);
		ПанельФорм.Location = Кикс.Point(Число(ПанельФорм.Location.x) - Число(ПанельКомпонентов.Size.Width), Число(ПанельФорм.Location.y));
		Разделитель2.Location = Кикс.Point(Число(Разделитель2.Location.x) - Число(ПанельКомпонентов.Size.Width), Число(Разделитель2.Location.y)); 
		Разделитель2.Enabled = Ложь;
		Кнопка11.Pushed = Ложь;
		Кнопка11.BackColor = "Aqua";
	Иначе 
		ПанельФорм.Size = Кикс.Size((Число(ПанельФорм.Size.Width) - Число(ПанельКомпонентов.Size.Width)), ПанельФорм.Size.Height);
		ПанельФорм.Location = Кикс.Point(Число(ПанельФорм.Location.x) + Число(ПанельКомпонентов.Size.Width), Число(ПанельФорм.Location.y));
		Разделитель2.Location = Кикс.Point(Число(Разделитель2.Location.x) + Число(ПанельКомпонентов.Size.Width), Число(Разделитель2.Location.y));
		Разделитель2.Enabled = Истина;
		Кнопка11.Pushed = Истина;
		Кнопка11.BackColor = "White";
	КонецЕсли;
КонецФункции

Функция Кнопка12_Click()
	Видимость = ПанельСвойств.Visible;
	ПанельСвойств.Visible = НЕ Видимость;
	Если Видимость Тогда
		Панель2.Size = Кикс.Size((Число(ПанельСвойств.Size.Width) + Число(Панель2.Size.Width)), Панель2.Size.Height);
		Разделитель1.Location = Кикс.Point(Число(Разделитель1.Location.x) + Число(ПанельСвойств.Size.Width), Число(Разделитель1.Location.y)); 
		Разделитель1.Enabled = Ложь;
		Кнопка12.Pushed = Ложь;
		Кнопка12.BackColor = "Aqua";
	Иначе 
		Панель2.Size = Кикс.Size((Число(Панель2.Size.Width) - Число(ПанельСвойств.Size.Width)), Панель2.Size.Height);
		Разделитель1.Location = Кикс.Point(Число(Разделитель1.Location.x) - Число(ПанельСвойств.Size.Width), Число(Разделитель1.Location.y));
		Разделитель1.Enabled = Истина;
		Кнопка12.Pushed = Истина;
		Кнопка12.BackColor = "White";
	КонецЕсли;
КонецФункции

Функция Кнопка13_Click()
	Кикс.MessageBox.Show("Нажата: " + Кикс.Sender.Text);
КонецФункции

Функция ПанельФорм_OnResize()
	УстановитьШиринуФормы();
КонецФункции

Функция ПанельКомпонентов_OnResize()
	УстановитьШиринуФормы();
КонецФункции

Функция УстановитьШиринуФормы()
	Ширина = Число(ПанельСвойств.Size.Width);
	Высота = Число(ПанельКомпонентов.Size.Width);
	Размер = Кикс.Size((Ширина + Высота + 40), 200);
	Форма1.MinimumSize = Размер;
КонецФункции

Процедура ПриОткрытии()
	Стр201601151504 = "UEsDBBQAAgAIAHmtL0glqYJS2gcAAJMVAAASAAAAU3VwcG9ydEtpWGZvcm1zLm9zzVhbbxvHFX62AP2HBYECZDKiRMdWUwh6EKlIFixWgihUCYogGHFH4obLGXq5K5lFHhQLddAmRVQjTtxATaKgSB6KorJrx7JlyX9h+EvyF/LNhXuhRCZt8xA+zJ4zc843Z85lLvz+9OXk5Cs/z298bHJydnZWHslv5H35d3kgv5IP5N8c+YX8KzqO5Jdov9VDD+S/HYweYOAL+bX8p3Nz6c2FlbVqDfpXp0rTU6XS9dL1qWuzmd/4GCCe9PbkE/nCqTWY7zvyX/J57335Up739np3iAOwzzD9I4jdkccXhz+XJ6pHDaqeAYGZ8THM8Y/evjxD91150vvYga1Pb7Db8t47a4vlPNDPoPqBowh0F8bHrshPnVkHizqXj3ofymeOPITE+0A9kY9m1Pgn8lQBHcxOTDtYwbkzUXJgJCyRpxgHQBEy5/KhPIblJ707vY/y8gifPaAey2ew9iSfmypdfe3a9elfv/6buXJl/o2FHIGM8sXjvDWGTMgDUioUJkoFPe/nwDyDJ+72J5PHuv/wR+w9vGhOXn76+9Lbr5SmC6+Cmnq7MErwtVjw6mjB67HgNSt4D1JPYdceTEJ0DtGZWkYqMvJkSKzmWR2xgjeysUK3jtVXcNhTrD+Xm3HSsUHXsOj0VWJ3XxKK/hy/Kk07rzol4pQK+BpVPdEVK6BwvkHvad52TMIDxpRh4RrwicH87/yCZcnHcD+yXNG9P4L/Djj/wTeulry8h3x4qOaB7pnKjf5gYaBQ4izKFkfW/cPRCjMDrp8yns9Wb1Gv8BQr+AAdqmYfyfN8wZlwBgJkSgW6yqIBCLXy094+IGxdHejJU0pIzd5fIPORUsAyFAgqQi03P5GXh8h2TJmWt/BrBeKMHtc5kMEqDZFdtFjDxy9iXR0iW7ZYw8dHbw8DHjwyIYf/9tSWoZIQfswNeI" +
	"048bITcjEhy7lfQtDvI4WBrXa/+07+spgZux5cGoMRYyprpgrKkHMU1WN5rKa7ULo/bmYMiFp60fvYWJ0Eyto/Inwjd4UjcHvgXsKYzBFQWaniIH7Y+zP2hOe9O/lctVarB147rAgeBsIvZjgdyjRYcZny7YhuM7Wx7mx2tHBOCylPqd7xsfcWIl4PPcGdRRbe9G7nsR+/d6XGQgcMRCoBoyFb2XyX1cN8LofOkAZhsdbthKyVy8XSRhsKSg+db3DX6UPnZq4MWDbnuhXhMhvrgvFQ9g6gdvaUxlrE8zkziV7o+Ji5aox22IZZdFHLmlxX9yFHn3pP9M75RAUaNxe1iSJFHuurzHdQ33cQI5sVsC97hcke1V9jA3ioy+MuKHvAwYYzID/LODybSKTPqmtCUqlJocaJHFPYyclAkWdCaqvgM9jxwliQuriQHFH3E7VeVcDA+FDl+5EOQGoPKNk9wGB9CwyV4OeWP5fP+4FLF//lO4ZK++fyZOhVaqDakil6+32jUYCFbA1lSiv27Zzv1VnZjxiZ46F3K2IbDS8EcyuiumnRwOPg/xAFjJSZt43W60COlH1ab6qW1xvMnfNbgrtEI6nmd57wWUjKgdjlpBwFfndDCJdUqMtCLVRpoCQCFnUUKerCR8WQigior1q+5YtdFhhJsB3Pb5JK4LU6gpNKl3IyT4OmHlZE3LMofJfxADNpLqBdSzBmBG42aNPTVBVFzkOq6RXf22GJ0EqATYBZst7wDNoaM98aVWs1JKOJVk0tITZJc7EB61FwKxJex4xZ58wz1l71eFMTtWbX6HotoyXcbeuABS9gm4EHby/4ykEmRAsiYJ3QTL8Q1Rsdj5JF6vHOpggEWWyITmgElU9I4hiNbtR0+xaqXOySG4KzrssUEWqjlrjrUa4WrahtQZZ2RNAlxoHLdIdxlwUxAUM7DXC73EAvM/io0vC2tuCqZW+7YaKuKRNlQ6rAaSo20NpjOpWtltKoitTW" +
	"acpGwtD9UBjOetMwcSgMGzLmJ6PxfC2mmz4KR9vPkSoNBCaqMteLWqmyMB0ay5A2XQyzGgVtvz8S22fZOFks3wbgdloiSRnDm6RR8ah6Lo89WvV4qM6bFqhO2F0TSkHU67TjcfJbukPfFSYNQHfJiu8u0zojOuFNOx/QTWIz3nzUHHYhq9RnSepoTpuoqMRAxSXmrdI27VJM2iarjNYbq9HWFqggIjpyq37UIqti17XZbb2kNGF81+wYa6JLTYxq1HV9ZnptvGuUu1Yu9ioIfV6Rmsc4p/j4O0jPfh4k7k6SocYR9rTfk8RYR1auMyTpegNOhXXrokVDQZIl2xLeaOCkJ8bBuq21RJMRm1PmY8DncKrvsLIIVNUYpkLb6qTPcuvsdkjm2u0NETQ7bRUre1Xpf9UGkqbTvE7oDJPp0djzrNMMRVvvA7rjBkR8LRZTpl+E64Ha45c4TRvfZ/vmD/Bad4lvCd1orsp4pBvN4QImfL9MA7LhcRduMp+FgKICDa3lMDmHBwJU4C/0tB48ngfvvImxP+3EHn5gq3ewfvK+kMclBTtw8yviNGjlBxf5/zxL/pdXScrGYhmZUxE4rxInpF8ZyaMgdUlTopdBzGTsKQ5oJFwaDrdD+6pPveMT0cIQTKuWcDND/iBIJIZBrennN97UQ8YXzXhp2Lh5vuPtO+oeh27kYO9PNhA54NzVRXGsXwHPLlzR8WRM/y3p4FF3jMfCPiCgpq/7P9vfqD8AUEsBAhQAFAACAAgAea0vSCWpglLaBwAAkxUAABIAAAAAAAAAAQAgAAAAAAAAAFN1cHBvcnRLaVhmb3Jtcy5vc1BLBQYAAAAAAQABAEAAAAAKCAAAAAA=";
	ДД = Base64Значение(Стр201601151504);
	ДД.Записать(КаталогВременныхФайлов() + "SupportKiXforms.zip");
	ЧтениеZip = Новый ЧтениеZipФайла(КаталогВременныхФайлов() + "SupportKiXforms.zip");
	ЧтениеZip.ИзвлечьВсе(КаталогВременныхФайлов(), РежимВосстановленияПутейФайловZIP.НеВосстанавливать);
	ЧтениеZip.Закрыть(); 
	ПодключитьСценарий(КаталогВременныхФайлов() + "SupportKiXforms.os", "СценарийKiXforms");
	ПоддержкаKiXforms = Новый СценарийKiXforms();
	Кикс = ПоддержкаKiXforms.Кикстарт;
	Если НЕ (Кикс.Version = "2.47.4.0") Тогда
		ЗавершитьРаботу(1);
	КонецЕсли;
	
	СоздатьФорму();
	
	Форма1.Center();
	Форма1.Visible = Истина;
	Форма1.Show();
	
	Пока Форма1.Visible Цикл
		Кикс = ПоддержкаKiXforms.Кикстарт; // очень важная строка, без неё GUI будет вылетать
		Действие = Кикс.DoEvents();
		Форма1.Enabled = Ложь;
		Кикс.Sender.Enabled = Ложь;
		ВыборФункции(Действие);
		Форма1.Enabled = Истина;
		Кикс.Sender.Enabled = Истина;
		Кикс.Sender.SetFocus();
		ПоддержкаKiXforms.Shell.SendKeys(""); // без этой строки, в некоторых случаях, форма будет реагировать на события с задержкой!
		Действие = "";
	КонецЦикла;
КонецПроцедуры

ПриОткрытии();
//***********************************************************************
