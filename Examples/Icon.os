﻿//***********************************************************************
Перем ПоддержкаKiXforms; // переменная, необходимая всегда для поддержки KiXforms
Перем Кикс, Форма1; // переменные для этого сценария

Процедура СоздатьФорму()
	Форма1 = Кикс.Form();
	Форма1.Text = "Пример: Icon (в строке заголовка)";
	
	Кнопка1 = Форма1.Button();
	Кнопка1.Size = Кикс.Size(150, 50);
	Кнопка1.Center();
	Кнопка1.Icon =  "SHELL32.DLL;10";
	
	//Иконка1 = Кикс.Icon( "C:\Иконка.ico" ); // вариант с файлом иконки на диске
	Стр = "AAABAAEAICAAAAEAGACoDAAAFgAAACgAAAAgAAAAQAAAAAEAGAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUEBAAAABhUkqMc2qxjo6fgICAZmYkHR0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA1Kyd4YVjEnpffsbBIOTitjoLOqZj5xcP/y8v/ysp9Y2MAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAUFOhfXbEopLyvbn/yMn/x8fZqKjGpJbUrJ39xMT/x8f/zc2GY2QnHhx4ZVmffHhJOTkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACtioDOrZntuLH/xcb/w8P/w8Tnt67qt6//w8T/w8P/x8fCk5KZfnHitKf/w8TttbVxVlYDAgIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAdGRYmHBtkSkjBppLcrqH/wsL/wMD/wMD/wcH/wMD/wMD/wMD/wMD+wL/vuLL9vr3/wMD/wMD9vr5QPDwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACIbGWrjX7aoqCqfHzEpZTUqpr+u7r/vL3/vLz/vLz/vLz/vLz/vLz/vLz/vL3/vb7/vb3/vLz/vLzkqKg4KioAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB6VVTEpJ" + "LlraT/ubr/urv4t7X1tbH/ubn/ubn/ubn/ubn/ubr/ubr/urv/ubr/ubn/ubn/ubn/ubn/ubmvf38AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACwjILRq5n5sq//trf/tbX/trb/trb/tbX/tbX/trb+tLT2sbDwtK7wsav4s7D/trf/tbb/tbX/tbX/tbWcb28UEA9ROzlsS0sIBgYAAAAAAAAAAAAAAAAAAAAAAACXY2TAnZDPq5jxraf/s7T/srL/srL/srL/srL/srL3rKzGiorFkY63j4fBnZDOqJjeq574r6z/srP/srL/srL1qqrZmZjvqKf+sLBVOzsAAAAAAAAAAAAAAAAAAAAAAACVX2Ghc3DBoJLNqJXxqqX/r6//rq7/rq7/rq7soaFtS0sAAAAAAAAAAAAAAACsg3zEppbNppTuqaP/r7D/rq7/rq7/rq//r6//rq7HiIgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABtUU+/pZTPpZP5qqj/rKz/rKz+q6uLXl4AAAAAAAAAAAAAAAAAAAAAAAC5lIvFppbNp5T0qKX/rK3/rKz/rKz/rKz/rKz6qalXOjoAAAAAAAAAAAAAAAAAAABMQTtBMS1JLzC0jYPiqJz8p6X/qKj/qKjZj48AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACwhX/HqpjVo5T+p6f/qKj/qKj/qKj/qKjJhIRAKioAAAAAAAAAAAAAAAAAAAC9oJDPno//p6f/pqb/pab/paX/paX/paWTX18AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACWYmO5k4rIqpXropr/pab/paX/paWhaWkGBAQAAAAAAAAAAAAAAAAAAAAAAADCopLWopP/oaH/oaH/oaH/oaH/oaH/oaFMMTEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACfcW/LrpvXoJL/oaL/oaH/oaFiPT0AAAAAAAAAAAAA" + "AAAAAAAAAAAAAADAno/YoZL/np7/np7/np7/np7/np7/np5FKysAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACid3PDo5XNpJH9nZ7/np7/np7pkJClZmZoQEAAAAAAAAAAAAAAAAAAAADEo5TSopH7nZr/mpr/mpv/m5v/m5v/m5tGKioAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACldnO4lInOqZX2mpf/m5v/m5v/m5v/m5v/m5sAAAAAAAAAAAAAAAAAAAC+mY++nY/Fno/Wo5Xom5P9mJf+mJj+mJh4SEgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACsiIDPq5jzl5P/mJj+mJj+mJj+mJj+mJgAAAAAAAAAAAAAAAAAAAAAAAAAAAB2VlS4nY7NpZLzlJH8lJT6lJSwaGgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACFXFuxjoTPqZX2k5L7lJT6lJT7lJT8k5T7k5QAAAAAAAAAAAAAAAAAAAAAAAAAAABhT0m8lojpl5D3kZD3kZH3kZHqioolFRUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACPb2m6nI7UoJD4kJD3kZH4kZHsj43ejYnlkYwAAAAAAAAAAAAAAAAAAAAAAACUeHDFnY7rko72jI30jo70jo70jo70jo6jX18AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACEYl/HrJjilIv1jY70jo73i41xUUwdGxcAAAAAAAAAAAAAAAAAAAAAAACebGzDpZXRn47viYnxiorwiorwiorwiorwiorrh4dqPT0AAAAAAAAAAAAAAAAAAAAAAAAAAACjhnzRoZHtionxiorwiorwiYmTV1YAAAAAAAAAAAAAAAAAAAAAAAAAAACXZGS4k4nKqpbgi4buhYbthobuhYbthobshobshobphIRzQkIAAAAAAAAAAAAAAAAAAACAaGHPopLmiobuhobsho" + "bshobshobuhoaTUlMCAQEAAAAAAAAAAAAAAAAAAAAAAACidHLJq5nQl4npgoLnhoXdkorfioTqgoPpg4Ppg4Ppg4OvYmJRLS0LBgYOCwp8YVnQkYjlhoPqgoPpg4Ppg4Ppg4Ppg4Ppg4PmgYFhNjYAAAAAAAAAAAAAAAAAAAAAAACZZma/m5DCoY7Jc3CycW/AppXOmorjf37lf3/lf3/lf3/lgIDjf3/benrafHvkgoHmf3/mfn/lf3/lf4Dkf3/lfX7mfn/lgH/aenpLKioAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACPY2G/o5PSkIXjeXrhe3vhe3vhe3vhe3vhe3vhe3vie3vie3vhe3vhe3vhe3vieXrZjIbRnI3bhH/ienuwYWEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACgfXbLp5XWgXvfdnfeeHjeeHjednbfdnfeeHjeeHjeeHjeeHjed3feeHjeeHjgd3euaWiUe3C/nIvJgnqJSkoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACVaWi6mYzLoY7YdXPbc3TadHTadXXTjYPTgXradHTadHTadHTadXXYeHbadHTadHTadHS8YmMTCwwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACfbm23kIfLrZnMjoHVcXDZb3C3ZmbKrZvIk4PWcHDXcHHXbm/WgHzKoI7ReXTYb3DXcXHYcXFPKisAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACYZGW4korGqJfHlIXDa2h7SUq/pZbJmIfSb23TbG3Va2vAcnC/qZfJlYXTbGzTbW3JaGiFRUUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAnJK+o5CPWVMAAAC/oZTJn4zObGrPZmbS" +  "ZmeFSkqfgnnLrZrHfnW9XV6gUlIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC7gHy6l43FpJLFhnzKfnfJe3OYV1VsS0zGrZylcGYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/gAf//gAA//wAAH/8AAA/+AAAH/AAAB/gAAAH4AAAA8AAAAPAAAADwAOAAcAHwAHAB+ADwAfgA8AH8AHAB/ABwAfwAcAH8AHwA/AB4APwA8AB8AfAAAADwAAAAeAAAAHgAAAD/gAAA/wAAAf8AAAf/AAAH/4AAD//gAB///AB/w==";
	ДД = Base64Значение(Стр);
	ДД.Записать(КаталогВременныхФайлов() + "Иконка.ico");
	Иконка1 = Кикс.Icon(КаталогВременныхФайлов() + "Иконка.ico");
	Форма1.Icon = Иконка1;
КонецПроцедуры

Процедура ВыборФункции(Действие)
	ПоддержкаKiXforms.Shell.SendKeys(""); // без этой строки, в некоторых случаях, форма будет реагировать на события с задержкой!
	Если Действие = "Имя_функции_обработчика_события_которое_пишется_вместе_со_скобками()" Тогда
		Имя_функции_обработчика_события_которое_пишется_вместе_со_скобками();
	КонецЕсли; 
КонецПроцедуры

Функция Имя_функции_обработчика_события_которое_пишется_вместе_со_скобками()
	// какие то действия
КонецФункции

Процедура ПриОткрытии()
	Стр201601151504 = "UEsDBBQAAgAIAHmtL0glqYJS2gcAAJMVAAASAAAAU3VwcG9ydEtpWGZvcm1zLm9zzVhbbxvHFX62AP2HBYECZDKiRMdWUwh6EKlIFixWgihUCYogGHFH4obLGXq5K5lFHhQLddAmRVQjTtxATaKgSB6KorJrx7JlyX9h+EvyF/LNhXuhRCZt8xA+zJ4zc843Z85lLvz+9OXk5Cs/z298bHJydnZWHslv5H35d3kgv5IP5N8c+YX8KzqO5Jdov9VDD+S/HYweYOAL+bX8p3Nz6c2FlbVqDfpXp0rTU6XS9dL1qWuzmd/4GCCe9PbkE/nCqTWY7zvyX/J57335Up739np3iAOwzzD9I4jdkccXhz+XJ6pHDaqeAYGZ8THM8Y/evjxD91150vvYga1Pb7Db8t47a4vlPNDPoPqBowh0F8bHrshPnVkHizqXj3ofymeOPITE+0A9kY9m1Pgn8lQBHcxOTDtYwbkzUXJgJCyRpxgHQBEy5/KhPIblJ707vY/y8gifPaAey2ew9iSfmypdfe3a9elfv/6buXJl/o2FHIGM8sXjvDWGTMgDUioUJkoFPe/nwDyDJ+72J5PHuv/wR+w9vGhOXn76+9Lbr5SmC6+Cmnq7MErwtVjw6mjB67HgNSt4D1JPYdceTEJ0DtGZWkYqMvJkSKzmWR2xgjeysUK3jtVXcNhTrD+Xm3HSsUHXsOj0VWJ3XxKK/hy/Kk07rzol4pQK+BpVPdEVK6BwvkHvad52TMIDxpRh4RrwicH87/yCZcnHcD+yXNG9P4L/Djj/wTeulry8h3x4qOaB7pnKjf5gYaBQ4izKFkfW/cPRCjMDrp8yns9Wb1Gv8BQr+AAdqmYfyfN8wZlwBgJkSgW6yqIBCLXy094+IGxdHejJU0pIzd5fIPORUsAyFAgqQi03P5GXh8h2TJmWt/BrBeKMHtc5kMEqDZFdtFjDxy9iXR0iW7ZYw8dHbw8DHjwyIYf/9tSWoZIQfswNeI" +
	"048bITcjEhy7lfQtDvI4WBrXa/+07+spgZux5cGoMRYyprpgrKkHMU1WN5rKa7ULo/bmYMiFp60fvYWJ0Eyto/Inwjd4UjcHvgXsKYzBFQWaniIH7Y+zP2hOe9O/lctVarB147rAgeBsIvZjgdyjRYcZny7YhuM7Wx7mx2tHBOCylPqd7xsfcWIl4PPcGdRRbe9G7nsR+/d6XGQgcMRCoBoyFb2XyX1cN8LofOkAZhsdbthKyVy8XSRhsKSg+db3DX6UPnZq4MWDbnuhXhMhvrgvFQ9g6gdvaUxlrE8zkziV7o+Ji5aox22IZZdFHLmlxX9yFHn3pP9M75RAUaNxe1iSJFHuurzHdQ33cQI5sVsC97hcke1V9jA3ioy+MuKHvAwYYzID/LODybSKTPqmtCUqlJocaJHFPYyclAkWdCaqvgM9jxwliQuriQHFH3E7VeVcDA+FDl+5EOQGoPKNk9wGB9CwyV4OeWP5fP+4FLF//lO4ZK++fyZOhVaqDakil6+32jUYCFbA1lSiv27Zzv1VnZjxiZ46F3K2IbDS8EcyuiumnRwOPg/xAFjJSZt43W60COlH1ab6qW1xvMnfNbgrtEI6nmd57wWUjKgdjlpBwFfndDCJdUqMtCLVRpoCQCFnUUKerCR8WQigior1q+5YtdFhhJsB3Pb5JK4LU6gpNKl3IyT4OmHlZE3LMofJfxADNpLqBdSzBmBG42aNPTVBVFzkOq6RXf22GJ0EqATYBZst7wDNoaM98aVWs1JKOJVk0tITZJc7EB61FwKxJex4xZ58wz1l71eFMTtWbX6HotoyXcbeuABS9gm4EHby/4ykEmRAsiYJ3QTL8Q1Rsdj5JF6vHOpggEWWyITmgElU9I4hiNbtR0+xaqXOySG4KzrssUEWqjlrjrUa4WrahtQZZ2RNAlxoHLdIdxlwUxAUM7DXC73EAvM/io0vC2tuCqZW+7YaKuKRNlQ6rAaSo20NpjOpWtltKoitTW" +
	"acpGwtD9UBjOetMwcSgMGzLmJ6PxfC2mmz4KR9vPkSoNBCaqMteLWqmyMB0ay5A2XQyzGgVtvz8S22fZOFks3wbgdloiSRnDm6RR8ah6Lo89WvV4qM6bFqhO2F0TSkHU67TjcfJbukPfFSYNQHfJiu8u0zojOuFNOx/QTWIz3nzUHHYhq9RnSepoTpuoqMRAxSXmrdI27VJM2iarjNYbq9HWFqggIjpyq37UIqti17XZbb2kNGF81+wYa6JLTYxq1HV9ZnptvGuUu1Yu9ioIfV6Rmsc4p/j4O0jPfh4k7k6SocYR9rTfk8RYR1auMyTpegNOhXXrokVDQZIl2xLeaOCkJ8bBuq21RJMRm1PmY8DncKrvsLIIVNUYpkLb6qTPcuvsdkjm2u0NETQ7bRUre1Xpf9UGkqbTvE7oDJPp0djzrNMMRVvvA7rjBkR8LRZTpl+E64Ha45c4TRvfZ/vmD/Bad4lvCd1orsp4pBvN4QImfL9MA7LhcRduMp+FgKICDa3lMDmHBwJU4C/0tB48ngfvvImxP+3EHn5gq3ewfvK+kMclBTtw8yviNGjlBxf5/zxL/pdXScrGYhmZUxE4rxInpF8ZyaMgdUlTopdBzGTsKQ5oJFwaDrdD+6pPveMT0cIQTKuWcDND/iBIJIZBrennN97UQ8YXzXhp2Lh5vuPtO+oeh27kYO9PNhA54NzVRXGsXwHPLlzR8WRM/y3p4FF3jMfCPiCgpq/7P9vfqD8AUEsBAhQAFAACAAgAea0vSCWpglLaBwAAkxUAABIAAAAAAAAAAQAgAAAAAAAAAFN1cHBvcnRLaVhmb3Jtcy5vc1BLBQYAAAAAAQABAEAAAAAKCAAAAAA=";
	ДД = Base64Значение(Стр201601151504);
	ДД.Записать(КаталогВременныхФайлов() + "SupportKiXforms.zip");
	ЧтениеZip = Новый ЧтениеZipФайла(КаталогВременныхФайлов() + "SupportKiXforms.zip");
	ЧтениеZip.ИзвлечьВсе(КаталогВременныхФайлов(), РежимВосстановленияПутейФайловZIP.НеВосстанавливать);
	ЧтениеZip.Закрыть(); 
	ПодключитьСценарий(КаталогВременныхФайлов() + "SupportKiXforms.os", "СценарийKiXforms");
	ПоддержкаKiXforms = Новый СценарийKiXforms();
	Кикс = ПоддержкаKiXforms.Кикстарт;
	Если НЕ (Кикс.Version = "2.47.4.0") Тогда
		ЗавершитьРаботу(1);
	КонецЕсли;
	
	СоздатьФорму();
	
	Форма1.Center();
	Форма1.Visible = Истина;
	Форма1.Show();
	
	Пока Форма1.Visible Цикл
		Кикс = ПоддержкаKiXforms.Кикстарт; // очень важная строка, без неё GUI будет вылетать
		Действие = Кикс.DoEvents();
		Форма1.Enabled = Ложь;
		Кикс.Sender.Enabled = Ложь;
		ВыборФункции(Действие);
		Форма1.Enabled = Истина;
		Кикс.Sender.Enabled = Истина;
		Кикс.Sender.SetFocus();
		ПоддержкаKiXforms.Shell.SendKeys(""); // без этой строки, в некоторых случаях, форма будет реагировать на события с задержкой!
		Действие = "";
	КонецЦикла;
КонецПроцедуры

ПриОткрытии();
//***********************************************************************
