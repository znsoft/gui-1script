﻿//***********************************************************************
Перем ПоддержкаKiXforms; // переменная, необходимая всегда для поддержки KiXforms
Перем Кикс, Форма1; // переменные для этого сценария

Процедура СоздатьФорму()
	Форма1 = Кикс.Form();
	Форма1.Text = "Пример: RadioButton";
	Форма1.Size = Кикс.Size(280, 400);
	Форма1.Resizable = Ложь;
	
	РамкаГруппы1 = Форма1.Controls.Add("GroupBox");
	РамкаГруппы1.Location = Кикс.Point(10, 10);
	РамкаГруппы1.Size = Кикс.Size(125, 115);
	
	Переключатель1 = РамкаГруппы1.Controls.Add("RadioButton");
	Переключатель1.Text = "Переключатель1";
	Переключатель1.Dock = "Left";
	Переключатель1.Width = 115;
	Переключатель1.Height = 15;
	Переключатель1.Top = 5;
	Переключатель1.Left = 5;
	Переключатель1.Checked = Истина;
	
	Переключатель2 = РамкаГруппы1.Controls.Add("RadioButton");
	Переключатель2.Text = "Переключатель2";
	Переключатель2.Dock = Переключатель1.Dock;
	Переключатель2.Width = Переключатель1.Width;
	Переключатель2.Height = Переключатель1.Height;
	Переключатель2.Top = Переключатель1.Height + 5;
	Переключатель2.Left = Переключатель1.Left;
	
	Переключатель3 = РамкаГруппы1.Controls.Add("RadioButton");
	Переключатель3.Text = "Переключатель3";
	Переключатель3.Dock = Переключатель1.Dock;
	Переключатель3.Width = Переключатель1.Width;
	Переключатель3.Height = Переключатель1.Height;
	Переключатель3.Top = ((Переключатель1.Height + 5) * 2) - 5;
	Переключатель3.Left = Переключатель1.Left;
	
	Переключатель4 = РамкаГруппы1.Controls.Add("RadioButton");
	Переключатель4.Text = "Переключатель4";
	Переключатель4.Dock = Переключатель1.Dock;
	Переключатель4.Width = Переключатель1.Width;
	Переключатель4.Height = Переключатель1.Height;
	Переключатель4.Top = ((Переключатель1.Height + 5) * 3) - 10;
	Переключатель4.Left = Переключатель1.Left;
	
	Переключатель5 = РамкаГруппы1.Controls.Add("RadioButton");
	Переключатель5.Text = "Переключатель5";
	Переключатель5.Dock = Переключатель1.Dock;
	Переключатель5.Width = Переключатель1.Width;
	Переключатель5.Height = Переключатель1.Height;
	Переключатель5.Top = ((Переключатель1.Height + 5) * 4) - 15;
	Переключатель5.Left = Переключатель1.Left;
	
	Переключатель6 = РамкаГруппы1.Controls.Add("RadioButton");
	Переключатель6.Text = "Переключатель6";
	Переключатель6.Dock = Переключатель1.Dock;
	Переключатель6.Width = Переключатель1.Width;
	Переключатель6.Height = Переключатель1.Height;
	Переключатель6.Top = ((Переключатель1.Height + 5) * 5) - 20;
	Переключатель6.Left = Переключатель1.Left;
	
	Переключатель7 = РамкаГруппы1.Controls.Add("RadioButton");
	Переключатель7.Text = "Переключатель7";
	Переключатель7.Dock = Переключатель1.Dock;
	Переключатель7.Width = Переключатель1.Width;
	Переключатель7.Height = Переключатель1.Height;
	Переключатель7.Top = ((Переключатель1.Height + 5) * 6) - 25;
	Переключатель7.Left = Переключатель1.Left;
	
	РамкаГруппы2 = Форма1.Controls.Add("GroupBox");
	РамкаГруппы2.Location = Кикс.Point(10, РамкаГруппы1.Bottom + 10);
	РамкаГруппы2.BorderStyle = 2;
	РамкаГруппы2.Size = Кикс.Size(255, 180);
	
	П1 = РамкаГруппы2.Controls.Add("RadioButton");
	П1.CheckAlign = 64;
	П1.Text = "П1";
	П1.Dock = "Top";
	П1.Width = 40;
	П1.Top = 5;
	П1.Left = 30;
	П1.Checked = Истина;
	
	П2 = РамкаГруппы2.Controls.Add("RadioButton");
	П2.CheckAlign = 64;
	П2.Text = "П2";
	П2.Dock = П1.Dock;
	П2.Width = П1.Width;
	П2.Left = 50;
	
	П3 = РамкаГруппы2.Controls.Add("RadioButton");
	П3.CheckAlign = 64;
	П3.Text = "П3";
	П3.Dock = П1.Dock;
	П3.Width = П1.Width;
	П3.Left = 70;
	
	П4 = РамкаГруппы2.Controls.Add("RadioButton");
	П4.CheckAlign = 64;
	П4.Text = "П4";
	П4.Dock = П1.Dock;
	П4.Width = П1.Width;
	П4.Left = 90;
	
	П5 = РамкаГруппы2.Controls.Add("RadioButton");
	П5.CheckAlign = 64;
	П5.Text = "П5";
	П5.Dock = П1.Dock;
	П5.Width = П1.Width;
	П5.Left = 70;
	
	П6 = РамкаГруппы2.Controls.Add("RadioButton");
	П6.CheckAlign = 64;
	П6.Text = "П6";
	П6.Dock = П1.Dock;
	П6.Width = П1.Width;
	П6.Left = 50;
	
	П7 = РамкаГруппы2.Controls.Add("RadioButton");
	П7.CheckAlign = 64;
	П7.Text = "П7";
	П7.Dock = П1.Dock;
	П7.Width = П1.Width;
	П7.Left = 30;
	
	П8 = РамкаГруппы2.Controls.Add("RadioButton");
	П8.Text = "П8";
	П8.Dock = П1.Dock;
	П8.Width = П1.Width;
	П8.Top = П1.Top;
	П8.Left = 178;
	
	П9 = РамкаГруппы2.Controls.Add("RadioButton");
	П9.Text = "П9";
	П9.Dock = П1.Dock;
	П9.Width = П1.Width;
	П9.Top = П2.Top;
	П9.Left = 158;
	
	П10 = РамкаГруппы2.Controls.Add("RadioButton");
	П10.Text = "П10";
	П10.Dock = П1.Dock;
	П10.Width = П1.Width;
	П10.Top = П3.Top;
	П10.Left = 138;
	
	П11 = РамкаГруппы2.Controls.Add("RadioButton");
	П11.Text = "П11";
	П11.Dock = П1.Dock;
	П11.Width = П1.Width;
	П11.Top = П5.Top;
	П11.Left = 138;
	
	П12 = РамкаГруппы2.Controls.Add("RadioButton");
	П12.Text = "П12";
	П12.Dock = П1.Dock;
	П12.Width = П1.Width;
	П12.Top = П6.Top;
	П12.Left = 158;
	
	П13 = РамкаГруппы2.Controls.Add("RadioButton");
	П13.Text = "П13";
	П13.Dock = П1.Dock;
	П13.Width = П1.Width;
	П13.Top = П7.Top;
	П13.Left = 178;
	
	РамкаГруппы3 = Форма1.Controls.Add("GroupBox");
	РамкаГруппы3.Location = Кикс.Point(10, РамкаГруппы2.Bottom + 10);
	РамкаГруппы3.Size = Кикс.Size(235, 25);
	
	Перекл1 = РамкаГруппы3.Controls.Add("RadioButton");
	Перекл1.Text = "Перекл1";
	Перекл1.Dock = "Left";
	Перекл1.Width = 75;
	Перекл1.Height = 15;
	Перекл1.Top = 5;
	Перекл1.Left = 5;
	Перекл1.Checked = Истина;
	
	Перекл2 = РамкаГруппы3.Controls.Add("RadioButton");
	Перекл2.Text = "Перекл2";
	Перекл2.Width = Перекл1.Width;
	Перекл2.Height = Перекл1.Height;
	Перекл2.Top = Перекл1.Top;
	Перекл2.Left = 80;
	
	Перекл3 = РамкаГруппы3.Controls.Add("RadioButton");
	Перекл3.Text = "Перекл3";
	Перекл3.Width = Перекл1.Width;
	Перекл3.Height = Перекл1.Height;
	Перекл3.Top = Перекл1.Top;
	Перекл3.Left = 155;
КонецПроцедуры

Процедура ВыборФункции(Действие)
	ПоддержкаKiXforms.Shell.SendKeys(""); // без этой строки, в некоторых случаях, форма будет реагировать на события с задержкой!
	Если Действие = "Имя_функции_обработчика_события_которое_пишется_вместе_со_скобками()" Тогда
		Имя_функции_обработчика_события_которое_пишется_вместе_со_скобками();
	ИначеЕсли Действие = "Какое_то_Событие()" Тогда
		Какое_то_Событие(); 
	КонецЕсли; 
КонецПроцедуры

// дальше идут функции - обработчики событий
Функция Имя_функции_обработчика_события_которое_пишется_вместе_со_скобками()
	// какие то действия
КонецФункции

Функция Какое_то_Событие()
	// какие то действия
КонецФункции

Процедура ПриОткрытии()
	Стр201601151504 = "UEsDBBQAAgAIAHmtL0glqYJS2gcAAJMVAAASAAAAU3VwcG9ydEtpWGZvcm1zLm9zzVhbbxvHFX62AP2HBYECZDKiRMdWUwh6EKlIFixWgihUCYogGHFH4obLGXq5K5lFHhQLddAmRVQjTtxATaKgSB6KorJrx7JlyX9h+EvyF/LNhXuhRCZt8xA+zJ4zc843Z85lLvz+9OXk5Cs/z298bHJydnZWHslv5H35d3kgv5IP5N8c+YX8KzqO5Jdov9VDD+S/HYweYOAL+bX8p3Nz6c2FlbVqDfpXp0rTU6XS9dL1qWuzmd/4GCCe9PbkE/nCqTWY7zvyX/J57335Up739np3iAOwzzD9I4jdkccXhz+XJ6pHDaqeAYGZ8THM8Y/evjxD91150vvYga1Pb7Db8t47a4vlPNDPoPqBowh0F8bHrshPnVkHizqXj3ofymeOPITE+0A9kY9m1Pgn8lQBHcxOTDtYwbkzUXJgJCyRpxgHQBEy5/KhPIblJ707vY/y8gifPaAey2ew9iSfmypdfe3a9elfv/6buXJl/o2FHIGM8sXjvDWGTMgDUioUJkoFPe/nwDyDJ+72J5PHuv/wR+w9vGhOXn76+9Lbr5SmC6+Cmnq7MErwtVjw6mjB67HgNSt4D1JPYdceTEJ0DtGZWkYqMvJkSKzmWR2xgjeysUK3jtVXcNhTrD+Xm3HSsUHXsOj0VWJ3XxKK/hy/Kk07rzol4pQK+BpVPdEVK6BwvkHvad52TMIDxpRh4RrwicH87/yCZcnHcD+yXNG9P4L/Djj/wTeulry8h3x4qOaB7pnKjf5gYaBQ4izKFkfW/cPRCjMDrp8yns9Wb1Gv8BQr+AAdqmYfyfN8wZlwBgJkSgW6yqIBCLXy094+IGxdHejJU0pIzd5fIPORUsAyFAgqQi03P5GXh8h2TJmWt/BrBeKMHtc5kMEqDZFdtFjDxy9iXR0iW7ZYw8dHbw8DHjwyIYf/9tSWoZIQfswNeI" +
	"048bITcjEhy7lfQtDvI4WBrXa/+07+spgZux5cGoMRYyprpgrKkHMU1WN5rKa7ULo/bmYMiFp60fvYWJ0Eyto/Inwjd4UjcHvgXsKYzBFQWaniIH7Y+zP2hOe9O/lctVarB147rAgeBsIvZjgdyjRYcZny7YhuM7Wx7mx2tHBOCylPqd7xsfcWIl4PPcGdRRbe9G7nsR+/d6XGQgcMRCoBoyFb2XyX1cN8LofOkAZhsdbthKyVy8XSRhsKSg+db3DX6UPnZq4MWDbnuhXhMhvrgvFQ9g6gdvaUxlrE8zkziV7o+Ji5aox22IZZdFHLmlxX9yFHn3pP9M75RAUaNxe1iSJFHuurzHdQ33cQI5sVsC97hcke1V9jA3ioy+MuKHvAwYYzID/LODybSKTPqmtCUqlJocaJHFPYyclAkWdCaqvgM9jxwliQuriQHFH3E7VeVcDA+FDl+5EOQGoPKNk9wGB9CwyV4OeWP5fP+4FLF//lO4ZK++fyZOhVaqDakil6+32jUYCFbA1lSiv27Zzv1VnZjxiZ46F3K2IbDS8EcyuiumnRwOPg/xAFjJSZt43W60COlH1ab6qW1xvMnfNbgrtEI6nmd57wWUjKgdjlpBwFfndDCJdUqMtCLVRpoCQCFnUUKerCR8WQigior1q+5YtdFhhJsB3Pb5JK4LU6gpNKl3IyT4OmHlZE3LMofJfxADNpLqBdSzBmBG42aNPTVBVFzkOq6RXf22GJ0EqATYBZst7wDNoaM98aVWs1JKOJVk0tITZJc7EB61FwKxJex4xZ58wz1l71eFMTtWbX6HotoyXcbeuABS9gm4EHby/4ykEmRAsiYJ3QTL8Q1Rsdj5JF6vHOpggEWWyITmgElU9I4hiNbtR0+xaqXOySG4KzrssUEWqjlrjrUa4WrahtQZZ2RNAlxoHLdIdxlwUxAUM7DXC73EAvM/io0vC2tuCqZW+7YaKuKRNlQ6rAaSo20NpjOpWtltKoitTW" +
	"acpGwtD9UBjOetMwcSgMGzLmJ6PxfC2mmz4KR9vPkSoNBCaqMteLWqmyMB0ay5A2XQyzGgVtvz8S22fZOFks3wbgdloiSRnDm6RR8ah6Lo89WvV4qM6bFqhO2F0TSkHU67TjcfJbukPfFSYNQHfJiu8u0zojOuFNOx/QTWIz3nzUHHYhq9RnSepoTpuoqMRAxSXmrdI27VJM2iarjNYbq9HWFqggIjpyq37UIqti17XZbb2kNGF81+wYa6JLTYxq1HV9ZnptvGuUu1Yu9ioIfV6Rmsc4p/j4O0jPfh4k7k6SocYR9rTfk8RYR1auMyTpegNOhXXrokVDQZIl2xLeaOCkJ8bBuq21RJMRm1PmY8DncKrvsLIIVNUYpkLb6qTPcuvsdkjm2u0NETQ7bRUre1Xpf9UGkqbTvE7oDJPp0djzrNMMRVvvA7rjBkR8LRZTpl+E64Ha45c4TRvfZ/vmD/Bad4lvCd1orsp4pBvN4QImfL9MA7LhcRduMp+FgKICDa3lMDmHBwJU4C/0tB48ngfvvImxP+3EHn5gq3ewfvK+kMclBTtw8yviNGjlBxf5/zxL/pdXScrGYhmZUxE4rxInpF8ZyaMgdUlTopdBzGTsKQ5oJFwaDrdD+6pPveMT0cIQTKuWcDND/iBIJIZBrennN97UQ8YXzXhp2Lh5vuPtO+oeh27kYO9PNhA54NzVRXGsXwHPLlzR8WRM/y3p4FF3jMfCPiCgpq/7P9vfqD8AUEsBAhQAFAACAAgAea0vSCWpglLaBwAAkxUAABIAAAAAAAAAAQAgAAAAAAAAAFN1cHBvcnRLaVhmb3Jtcy5vc1BLBQYAAAAAAQABAEAAAAAKCAAAAAA=";
	ДД = Base64Значение(Стр201601151504);
	ДД.Записать(КаталогВременныхФайлов() + "SupportKiXforms.zip");
	ЧтениеZip = Новый ЧтениеZipФайла(КаталогВременныхФайлов() + "SupportKiXforms.zip");
	ЧтениеZip.ИзвлечьВсе(КаталогВременныхФайлов(), РежимВосстановленияПутейФайловZIP.НеВосстанавливать);
	ЧтениеZip.Закрыть(); 
	ПодключитьСценарий(КаталогВременныхФайлов() + "SupportKiXforms.os", "СценарийKiXforms");
	ПоддержкаKiXforms = Новый СценарийKiXforms();
	Кикс = ПоддержкаKiXforms.Кикстарт;
	Если НЕ (Кикс.Version = "2.47.4.0") Тогда
		ЗавершитьРаботу(1);
	КонецЕсли;
	
	СоздатьФорму();
	
	Форма1.Center();
	Форма1.Visible = Истина;
	Форма1.Show();
	
	Пока Форма1.Visible Цикл
		Кикс = ПоддержкаKiXforms.Кикстарт; // очень важная строка, без неё GUI будет вылетать
		Действие = Кикс.DoEvents();
		Форма1.Enabled = Ложь;
		Кикс.Sender.Enabled = Ложь;
		ВыборФункции(Действие);
		Форма1.Enabled = Истина;
		Кикс.Sender.Enabled = Истина;
		Кикс.Sender.SetFocus();
		ПоддержкаKiXforms.Shell.SendKeys(""); // без этой строки, в некоторых случаях, форма будет реагировать на события с задержкой!
		Действие = "";
	КонецЦикла;
КонецПроцедуры

ПриОткрытии();
//***********************************************************************
