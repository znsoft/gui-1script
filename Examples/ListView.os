﻿//***********************************************************************
Перем ПоддержкаKiXforms; // переменная, необходимая всегда для поддержки KiXforms
Перем Кикс, Форма1, СписокЭлементов1, Кнопка1, Кнопка2, Кнопка3, Кнопка4, Кнопка5, Кнопка6, Кнопка7; // переменные для этого сценария
Перем Кнопка8, Кнопка9, Кнопка10, Кнопка11, Кнопка12, ЧемЗаполнено, КолЗначков1, КолЗначков2; // переменные для этого сценария

Процедура СоздатьФорму()
	Форма1 = Кикс.Form();
	Форма1.Text = "Пример: ListView";
	Форма1.Size = Кикс.Size(640,580);
	Форма1.MinimumSize = Кикс.Size(300, 200);
	
	СписокЭлементов1 = Форма1.ListView();
	СписокЭлементов1.Dock = 5;
	СписокЭлементов1.Right = 500;
	СписокЭлементов1.MultiSelect = Истина;
	СписокЭлементов1.LabelEdit = Истина;
	
	Кнопка1 = Форма1.Button();
	Кнопка1.Text = "Цвета Кикс";
	Кнопка1.Left = 505;
	Кнопка1.Top = 10;
	Кнопка1.Width = 125;
	Кнопка1.Height = 35;
	Кнопка1.Anchor = 9;
	Кнопка1.OnClick = "ЗаполнитьЦветами()";
	
	Кнопка2 = Форма1.Button();
	Кнопка2.Text = "Значки Кикс";
	Кнопка2.Left = Кнопка1.Left;
	Кнопка2.Top = Кнопка1.Bottom + 5;
	Кнопка2.Width = Кнопка1.Width;
	Кнопка2.Height = Кнопка1.Height;
	Кнопка2.Anchor = Кнопка1.Anchor;
	Кнопка2.OnClick = "ЗначкиКикс()";
	
	Кнопка3 = Форма1.Button();
	Кнопка3.Text = "Добавить колонку (для Details)";
	Кнопка3.Left = Кнопка1.Left;
	Кнопка3.Top = Кнопка2.Bottom + 5;
	Кнопка3.Width = Кнопка1.Width;
	Кнопка3.Height = Кнопка1.Height;
	Кнопка3.Anchor = Кнопка1.Anchor;
	Кнопка3.OnClick = "Последняя()";
	
	Кнопка4 = Форма1.Button();
	Кнопка4.Text = "Сообщить выделенное";
	Кнопка4.Left = Кнопка1.Left;
	Кнопка4.Top = Кнопка3.Bottom + 5;
	Кнопка4.Width = Кнопка1.Width;
	Кнопка4.Height = Кнопка1.Height;
	Кнопка4.Anchor = Кнопка1.Anchor;
	Кнопка4.OnClick = "СообщитьВыделенное()";
	
	Кнопка5 = Форма1.Button();
	Кнопка5.Text = "Сообщить фокус";
	Кнопка5.Left = Кнопка1.Left;
	Кнопка5.Top = Кнопка4.Bottom + 5;
	Кнопка5.Width = Кнопка1.Width;
	Кнопка5.Height = Кнопка1.Height;
	Кнопка5.Anchor = Кнопка1.Anchor;
	Кнопка5.OnClick = "СообщитьФокус()";
	
	Кнопка6 = Форма1.Button();
	Кнопка6.Text = "Отображение LargeIcon";
	Кнопка6.Left = Кнопка1.Left;
	Кнопка6.Top = Кнопка5.Bottom + 5;
	Кнопка6.Width = Кнопка1.Width;
	Кнопка6.Height = Кнопка1.Height;
	Кнопка6.Anchor = Кнопка1.Anchor;
	Кнопка6.OnClick = "LargeIcon()";
	
	Кнопка7 = Форма1.Button();
	Кнопка7.Text = "Отображение Details";
	Кнопка7.Left = Кнопка1.Left;
	Кнопка7.Top = Кнопка6.Bottom + 5;
	Кнопка7.Width = Кнопка1.Width;
	Кнопка7.Height = Кнопка1.Height;
	Кнопка7.Anchor = 9;
	Кнопка7.OnClick = "Details()";
	
	Кнопка8 = Форма1.Button();
	Кнопка8.Text = "Отображение SmallIcon";
	Кнопка8.Left = Кнопка1.Left;
	Кнопка8.Top = Кнопка7.Bottom + 5;
	Кнопка8.Width = Кнопка1.Width;
	Кнопка8.Height = Кнопка1.Height;
	Кнопка8.Anchor = Кнопка1.Anchor;
	Кнопка8.OnClick = "SmallIcon()";
	
	Кнопка9 = Форма1.Button();
	Кнопка9.Text = "Отображение List";
	Кнопка9.Left = Кнопка1.Left;
	Кнопка9.Top = Кнопка8.Bottom + 5;
	Кнопка9.Width = Кнопка1.Width;
	Кнопка9.Height = Кнопка1.Height;
	Кнопка9.Anchor = Кнопка1.Anchor;
	Кнопка9.OnClick = "List()";
	
	Кнопка10 = Форма1.Button();
	Кнопка10.Text = "Сетка (для Details)";
	Кнопка10.Left = Кнопка1.Left;
	Кнопка10.Top = Кнопка9.Bottom + 5;
	Кнопка10.Width = Кнопка1.Width;
	Кнопка10.Height = Кнопка1.Height;
	Кнопка10.Anchor = Кнопка1.Anchor;
	Кнопка10.OnClick = "GridLines()";
	
	Кнопка11 = Форма1.Button();
	Кнопка11.Text = "С отметками";
	Кнопка11.Left = Кнопка1.Left;
	Кнопка11.Top = Кнопка10.Bottom + 5;
	Кнопка11.Width = Кнопка1.Width;
	Кнопка11.Height = Кнопка1.Height;
	Кнопка11.Anchor = Кнопка1.Anchor;
	Кнопка11.OnClick = "СОтметками()";
	
	Кнопка12 = Форма1.Button();
	Кнопка12.Text = "Значки SHELL32.DLL";
	Кнопка12.Left = Кнопка1.Left;
	Кнопка12.Top = Кнопка11.Bottom + 5;
	Кнопка12.Width = Кнопка1.Width;
	Кнопка12.Height = Кнопка1.Height;
	Кнопка12.Anchor = Кнопка1.Anchor;
	Кнопка12.OnClick = "Значки_SHELL32_DLL()";
	
	SmallImageList = Кикс.ImageList();
	Для А = 0 По 500 Цикл
		SmallImageList.Images.Add(Кикс.BuiltinIcons(А));
	КонецЦикла;
	КолЗначков1 = SmallImageList.Images.Count;
	
	ImageList = Кикс.ImageList();
	Для А = 0 По 300 Цикл
		Значок = "SHELL32.DLL;" + А;
		SmallImageList.Images.Add(Значок);
	КонецЦикла;
	СписокЭлементов1.SmallImageList = SmallImageList;
	
	КолЗначков2 = SmallImageList.Images.Count - КолЗначков1;
	
	LargeImageList = Кикс.ImageList();
	LargeImageList.Size = Кикс.Size(32, 32);
	// первые КолЗначков1 это значки Кикс, остальные от SHELL32
	SmallImageListImagesCount = SmallImageList.Images.Count;
	Для А = 0 По SmallImageListImagesCount Цикл
		LargeImageList.Images.Add(SmallImageList.Images(А));
	КонецЦикла;
	СписокЭлементов1.LargeImageList = LargeImageList;
	
	ЗаполнитьЦветами();
	
	СписокЭлементов1.SetFocus();
	СписокЭлементов1.View = 1;
КонецПроцедуры

Процедура ВыборФункции(Действие)
	ПоддержкаKiXforms.Shell.SendKeys(""); // без этой строки, в некоторых случаях, форма будет реагировать на события с задержкой!
	Если Действие = "ЗаполнитьЦветами()" Тогда
		ЗаполнитьЦветами();
	ИначеЕсли Действие = "ЗначкиКикс()" Тогда
		ЗначкиКикс();
	ИначеЕсли Действие = "Последняя()" Тогда
		Последняя();
	ИначеЕсли Действие = "СообщитьВыделенное()" Тогда
		СообщитьВыделенное();
	ИначеЕсли Действие = "СообщитьФокус()" Тогда
		СообщитьФокус();
	ИначеЕсли Действие = "LargeIcon()" Тогда
		LargeIcon();
	ИначеЕсли Действие = "Details()" Тогда
		Details();
	ИначеЕсли Действие = "SmallIcon()" Тогда
		SmallIcon();
	ИначеЕсли Действие = "List()" Тогда
		List();
	ИначеЕсли Действие = "GridLines()" Тогда
		GridLines();
	ИначеЕсли Действие = "СОтметками()" Тогда
		СОтметками();
	ИначеЕсли Действие = "Значки_SHELL32_DLL()" Тогда
		Значки_SHELL32_DLL();
	КонецЕсли;
	СписокЭлементов1.SetFocus();
КонецПроцедуры

Функция ЗаполнитьЦветами()
	СписокЭлементов1.Clear();
	СписокЭлементов1.Columns.Add("Цвет",120,0);
	СписокЭлементов1.Columns.Add("Hex",70,0);
	СписокЭлементов1.Columns.Add("R",50,0);
	СписокЭлементов1.Columns.Add("G",50,0);
	СписокЭлементов1.Columns.Add("B",50,0);
	СписокЭлементов1.Columns.Add("Dec",70,0);
	
	ПоддержкаKiXformsТЗЦветаКоличество = ПоддержкаKiXforms.ТЗЦвета.Количество();
	Для А = 0 По (ПоддержкаKiXformsТЗЦветаКоличество - 1) Цикл
		Кикс = ПоддержкаKiXforms.Кикстарт; // очень важная строка, без неё GUI будет вылетать 
		СтрТЗЦвета = ПоддержкаKiXforms.ТЗЦвета.Получить(А);
		СписокЭлементов1.Items.Add(); // вторым параметром идёт номер значка в списке определенном в свойствах SmallImageList и LargeImageList
		СписокЭлементов1.Items(А).SubItems(0).Text = СтрТЗЦвета.ЦветИмя;
		СписокЭлементов1.Items(А).SubItems(1).Text = СтрТЗЦвета.ЦветHex;
		СписокЭлементов1.Items(А).SubItems(2).Text = СтрТЗЦвета.ЦветR;
		СписокЭлементов1.Items(А).SubItems(3).Text = СтрТЗЦвета.ЦветG;
		СписокЭлементов1.Items(А).SubItems(4).Text = СтрТЗЦвета.ЦветB;
		СписокЭлементов1.Items(А).SubItems(5).Text = СтрТЗЦвета.ЦветDec;
	КонецЦикла;
	
	ЧемЗаполнено = "Цветы";
	Кнопка3.Text = "Добавить колонку (для Details)";
	Форма1.Refresh();
КонецФункции

Функция ЗначкиКикс()
	СписокЭлементов1.Clear();
	СписокЭлементов1.Columns.Add("Номер",70,0);
	
	Для А = 0 По (КолЗначков1 - 1) Цикл
		СписокЭлементов1.Items.Add(А, А); // вторым параметром идёт номер значка в списке определенном в свойствах SmallImageList и LargeImageList
		СписокЭлементов1.Items.SubItems(0).Text = А;
	КонецЦикла;
	
	ЧемЗаполнено = "ЗначкиКикс";
	Кнопка3.Text = "Добавить колонку (для Details)";
КонецФункции

Функция Значки_SHELL32_DLL()
	СписокЭлементов1.Clear();
	СписокЭлементов1.Columns.Add("Номер",70,0);
	
	Для А = 0 По (КолЗначков2 - 1) Цикл
		СписокЭлементов1.Items.Add(А, А + КолЗначков1); // вторым параметром идёт номер значка в списке определенном в свойствах SmallImageList и LargeImageList
		СписокЭлементов1.Items.SubItems(0).Text = А;
	КонецЦикла;
	
	ЧемЗаполнено = "Значки_SHELL32_DLL";
	Кнопка3.Text = "Добавить колонку (для Details)";
КонецФункции


Функция Последняя()
	Если Кнопка3.Text = "Добавить колонку (для Details)" Тогда
		СписокЭлементов1.Columns.Add("Последняя",80,0);
		Кнопка3.Text = "Удалить последнюю колонку";
	Иначе
		СписокЭлементов1.Columns.Remove(СписокЭлементов1.Columns.Count-1);
		Кнопка3.Text = "Добавить колонку (для Details)";
	КонецЕсли;
КонецФункции

Функция СообщитьВыделенное()
	Если СписокЭлементов1.SelectedItems.Count > 0 Тогда
		Стр = "" + Символы.ПС;
		Для А = 0 По СписокЭлементов1.Items.Count - 1 Цикл
			Если СписокЭлементов1.Items(А).Selected() <> 0 Тогда  
				Стр = Стр + " " + СписокЭлементов1.Items(А).SubItems(0).Text + "  " + СписокЭлементов1.Items(А).SubItems(1).Text + Символы.ПС;
			КонецЕсли;
		КонецЦикла;
		Кикс.MessageBox.Show("Выбрано: " + Стр);
	КонецЕсли;
КонецФункции

Функция СообщитьФокус()
	Если СписокЭлементов1.FocusedItem <> Неопределено Тогда
		Кикс.MessageBox.Show("В фокусе: " + СписокЭлементов1.FocusedItem.Text);
	КонецЕсли;
КонецФункции

Функция LargeIcon()
	СписокЭлементов1.View = "LargeIcon";
КонецФункции

Функция Details()
	Если СписокЭлементов1.View = "1" Тогда
		Возврат Неопределено;
	КонецЕсли;
	СписокЭлементов1.View = "Details";
	// устраняем небольшой глюк при переключении режима отображения, нужно создать колонки
	Если ЧемЗаполнено = "Цветы" Тогда
		ЗаполнитьЦветами();
	ИначеЕсли ЧемЗаполнено = "ЗначкиКикс" Тогда
		ЗначкиКикс();
	ИначеЕсли ЧемЗаполнено = "Значки_SHELL32_DLL" Тогда
		Значки_SHELL32_DLL();
	КонецЕсли;
КонецФункции

Функция SmallIcon()
	СписокЭлементов1.View = "SmallIcon";
КонецФункции

Функция List()
	СписокЭлементов1.View = "List";
КонецФункции

Функция GridLines()
	Если СписокЭлементов1.GridLines Тогда
		СписокЭлементов1.GridLines = Ложь;
		Кнопка10.Text = "Сетка (для Details)";
	Иначе
		СписокЭлементов1.GridLines = Истина;
		Кнопка10.Text = "Без сетки (для Details)";
	КонецЕсли;
КонецФункции

Функция СОтметками()
	Если СписокЭлементов1.CheckBoxes Тогда
		СписокЭлементов1.CheckBoxes = Ложь;
		Кнопка11.Text = "С отметками";
	Иначе
		СписокЭлементов1.CheckBoxes = Истина;
		Кнопка11.Text = "Без отметок";
	КонецЕсли;
КонецФункции

Процедура ПриОткрытии()
	Стр201601151504 = "UEsDBBQAAgAIAHmtL0glqYJS2gcAAJMVAAASAAAAU3VwcG9ydEtpWGZvcm1zLm9zzVhbbxvHFX62AP2HBYECZDKiRMdWUwh6EKlIFixWgihUCYogGHFH4obLGXq5K5lFHhQLddAmRVQjTtxATaKgSB6KorJrx7JlyX9h+EvyF/LNhXuhRCZt8xA+zJ4zc843Z85lLvz+9OXk5Cs/z298bHJydnZWHslv5H35d3kgv5IP5N8c+YX8KzqO5Jdov9VDD+S/HYweYOAL+bX8p3Nz6c2FlbVqDfpXp0rTU6XS9dL1qWuzmd/4GCCe9PbkE/nCqTWY7zvyX/J57335Up739np3iAOwzzD9I4jdkccXhz+XJ6pHDaqeAYGZ8THM8Y/evjxD91150vvYga1Pb7Db8t47a4vlPNDPoPqBowh0F8bHrshPnVkHizqXj3ofymeOPITE+0A9kY9m1Pgn8lQBHcxOTDtYwbkzUXJgJCyRpxgHQBEy5/KhPIblJ707vY/y8gifPaAey2ew9iSfmypdfe3a9elfv/6buXJl/o2FHIGM8sXjvDWGTMgDUioUJkoFPe/nwDyDJ+72J5PHuv/wR+w9vGhOXn76+9Lbr5SmC6+Cmnq7MErwtVjw6mjB67HgNSt4D1JPYdceTEJ0DtGZWkYqMvJkSKzmWR2xgjeysUK3jtVXcNhTrD+Xm3HSsUHXsOj0VWJ3XxKK/hy/Kk07rzol4pQK+BpVPdEVK6BwvkHvad52TMIDxpRh4RrwicH87/yCZcnHcD+yXNG9P4L/Djj/wTeulry8h3x4qOaB7pnKjf5gYaBQ4izKFkfW/cPRCjMDrp8yns9Wb1Gv8BQr+AAdqmYfyfN8wZlwBgJkSgW6yqIBCLXy094+IGxdHejJU0pIzd5fIPORUsAyFAgqQi03P5GXh8h2TJmWt/BrBeKMHtc5kMEqDZFdtFjDxy9iXR0iW7ZYw8dHbw8DHjwyIYf/9tSWoZIQfswNeI" +
	"048bITcjEhy7lfQtDvI4WBrXa/+07+spgZux5cGoMRYyprpgrKkHMU1WN5rKa7ULo/bmYMiFp60fvYWJ0Eyto/Inwjd4UjcHvgXsKYzBFQWaniIH7Y+zP2hOe9O/lctVarB147rAgeBsIvZjgdyjRYcZny7YhuM7Wx7mx2tHBOCylPqd7xsfcWIl4PPcGdRRbe9G7nsR+/d6XGQgcMRCoBoyFb2XyX1cN8LofOkAZhsdbthKyVy8XSRhsKSg+db3DX6UPnZq4MWDbnuhXhMhvrgvFQ9g6gdvaUxlrE8zkziV7o+Ji5aox22IZZdFHLmlxX9yFHn3pP9M75RAUaNxe1iSJFHuurzHdQ33cQI5sVsC97hcke1V9jA3ioy+MuKHvAwYYzID/LODybSKTPqmtCUqlJocaJHFPYyclAkWdCaqvgM9jxwliQuriQHFH3E7VeVcDA+FDl+5EOQGoPKNk9wGB9CwyV4OeWP5fP+4FLF//lO4ZK++fyZOhVaqDakil6+32jUYCFbA1lSiv27Zzv1VnZjxiZ46F3K2IbDS8EcyuiumnRwOPg/xAFjJSZt43W60COlH1ab6qW1xvMnfNbgrtEI6nmd57wWUjKgdjlpBwFfndDCJdUqMtCLVRpoCQCFnUUKerCR8WQigior1q+5YtdFhhJsB3Pb5JK4LU6gpNKl3IyT4OmHlZE3LMofJfxADNpLqBdSzBmBG42aNPTVBVFzkOq6RXf22GJ0EqATYBZst7wDNoaM98aVWs1JKOJVk0tITZJc7EB61FwKxJex4xZ58wz1l71eFMTtWbX6HotoyXcbeuABS9gm4EHby/4ykEmRAsiYJ3QTL8Q1Rsdj5JF6vHOpggEWWyITmgElU9I4hiNbtR0+xaqXOySG4KzrssUEWqjlrjrUa4WrahtQZZ2RNAlxoHLdIdxlwUxAUM7DXC73EAvM/io0vC2tuCqZW+7YaKuKRNlQ6rAaSo20NpjOpWtltKoitTW" +
	"acpGwtD9UBjOetMwcSgMGzLmJ6PxfC2mmz4KR9vPkSoNBCaqMteLWqmyMB0ay5A2XQyzGgVtvz8S22fZOFks3wbgdloiSRnDm6RR8ah6Lo89WvV4qM6bFqhO2F0TSkHU67TjcfJbukPfFSYNQHfJiu8u0zojOuFNOx/QTWIz3nzUHHYhq9RnSepoTpuoqMRAxSXmrdI27VJM2iarjNYbq9HWFqggIjpyq37UIqti17XZbb2kNGF81+wYa6JLTYxq1HV9ZnptvGuUu1Yu9ioIfV6Rmsc4p/j4O0jPfh4k7k6SocYR9rTfk8RYR1auMyTpegNOhXXrokVDQZIl2xLeaOCkJ8bBuq21RJMRm1PmY8DncKrvsLIIVNUYpkLb6qTPcuvsdkjm2u0NETQ7bRUre1Xpf9UGkqbTvE7oDJPp0djzrNMMRVvvA7rjBkR8LRZTpl+E64Ha45c4TRvfZ/vmD/Bad4lvCd1orsp4pBvN4QImfL9MA7LhcRduMp+FgKICDa3lMDmHBwJU4C/0tB48ngfvvImxP+3EHn5gq3ewfvK+kMclBTtw8yviNGjlBxf5/zxL/pdXScrGYhmZUxE4rxInpF8ZyaMgdUlTopdBzGTsKQ5oJFwaDrdD+6pPveMT0cIQTKuWcDND/iBIJIZBrennN97UQ8YXzXhp2Lh5vuPtO+oeh27kYO9PNhA54NzVRXGsXwHPLlzR8WRM/y3p4FF3jMfCPiCgpq/7P9vfqD8AUEsBAhQAFAACAAgAea0vSCWpglLaBwAAkxUAABIAAAAAAAAAAQAgAAAAAAAAAFN1cHBvcnRLaVhmb3Jtcy5vc1BLBQYAAAAAAQABAEAAAAAKCAAAAAA=";
	ДД = Base64Значение(Стр201601151504);
	ДД.Записать(КаталогВременныхФайлов() + "SupportKiXforms.zip");
	ЧтениеZip = Новый ЧтениеZipФайла(КаталогВременныхФайлов() + "SupportKiXforms.zip");
	ЧтениеZip.ИзвлечьВсе(КаталогВременныхФайлов(), РежимВосстановленияПутейФайловZIP.НеВосстанавливать);
	ЧтениеZip.Закрыть(); 
	ПодключитьСценарий(КаталогВременныхФайлов() + "SupportKiXforms.os", "СценарийKiXforms");
	ПоддержкаKiXforms = Новый СценарийKiXforms();
	Кикс = ПоддержкаKiXforms.Кикстарт;
	Если НЕ (Кикс.Version = "2.47.4.0") Тогда
		ЗавершитьРаботу(1);
	КонецЕсли;
	
	СоздатьФорму();
	
	Форма1.Center();
	Форма1.Visible = Истина;
	Форма1.Show();
	
	Пока Форма1.Visible Цикл
		Кикс = ПоддержкаKiXforms.Кикстарт; // очень важная строка, без неё GUI будет вылетать
		Действие = Кикс.DoEvents();
		Форма1.Enabled = Ложь;
		Кикс.Sender.Enabled = Ложь;
		ВыборФункции(Действие);
		Форма1.Enabled = Истина;
		Кикс.Sender.Enabled = Истина;
		Кикс.Sender.SetFocus();
		ПоддержкаKiXforms.Shell.SendKeys(""); // без этой строки, в некоторых случаях, форма будет реагировать на события с задержкой!
		Действие = "";
	КонецЦикла;
КонецПроцедуры

ПриОткрытии();
//***********************************************************************
